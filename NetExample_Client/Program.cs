﻿using System;
using Kalan.Net;
using System.Threading;
using Lidgren.Network;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using Net.Net;


namespace NetExample_Client
{
    internal class Program
    {

        private static NetworkClient client;

        private static void Main(string[] args)
        {
            var afaf = new NetworkFactory("192.168.1.31", NetConfigConstants.PORT);
            client = (NetworkClient) afaf.CreateClient();
            Thread server_polling = new Thread(PollServer);
            server_polling.Start();
            client.JoinServer();

            var helloPacket = new HelloPacket();
            helloPacket.name = "akakak".ToCharArray().ToList();

            client.SendPacket(helloPacket, 0, SendOptions.Reliable);

            Thread.Sleep(2000);

            //Console.Clear();
            Console.SetCursorPosition(1, 1);
            Console.Write('A');

            var loop = true;
            byte[] up = {(byte) ClientStatus.Playing, 1};
            byte[] right = {(byte) ClientStatus.Playing, 2};
            byte[] down = {(byte) ClientStatus.Playing, 3};
            byte[] left = {(byte) ClientStatus.Playing, 4};

            while (loop)
            {

                var ch = Console.ReadKey(true).Key;
                switch (ch)
                {
                    case ConsoleKey.Escape:

                        loop = false;
                        break;

                    case ConsoleKey.UpArrow:
                        client.SendData(up, 0, SendOptions.None);
                        break;

                    case ConsoleKey.DownArrow:
                        client.SendData(down, 0, SendOptions.None);
                        break;

                    case ConsoleKey.LeftArrow:
                        client.SendData(left, 0, SendOptions.None);
                        break;

                    case ConsoleKey.RightArrow:
                        client.SendData(right, 0, SendOptions.None);
                        break;
                }
            }

            client.LeaveServer();
            server_polling.Interrupt();
            Thread.Sleep(2000);
            Environment.Exit(0);
        }

        private static int _prevx = 1, _prevy = 1, _x = 1, _y = 1;

        private static void WriteAt()
        {
            Console.SetCursorPosition(_prevx, _prevy);
            Console.Write('x');
            Console.SetCursorPosition(_x, _y);
            Console.Write('O');
            _prevx = _x;
            _prevy = _y;
        }

        public static void PollServer()
        {
            while (true)
            {
                var netMessage = client.Poll();
                if (netMessage == null) continue;
                /*
                if (data[0] == 0)
                {
                    switch (data[1])
                    {
                        case 1:
                            if (_y > 1) _y--;
                            break;

                        case 2:
                            if (_x < Console.WindowWidth - 2) _x++;
                            break;

                        case 3:
                            if (_y < Console.WindowHeight - 2) _y++;
                            break;

                        case 4:
                            if (_x > 1) _x--;
                            break;
                    }
                    WriteAt();
                }
            }
                 */
            }
        }
    }
}

