﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Physics;
using Protocol;
using ServerLogic.ShootingHandlers;
using WorldState;

namespace ServerLogic
{
    public class ServerLogicAPI
    {
        private Worldstate _worldstate;
        private Simulation _simulation;

        public ServerLogicAPI(Worldstate ws)
        {
            _worldstate = ws;
            _simulation = new Simulation(ws.PhysObjects);
            _simulation.Sync();
        }

        //TODO: This is obviously a terrible place, but spawning logic MUST be in ServerLogic, so let's make it so
        //General idea seems sound nevertheless so that will be a matter of a simple refactor
        public void OnPlayerSpawn(Player player)
        {
            PlayerObject po = new PlayerObject(Category.Players);

            _worldstate.AddPlayer(player, po);
            _simulation.Add(po);

            po.Position = _worldstate.Map.GetSensibleSpawnPosition();

            foreach (var pl in _worldstate.Players)
            {
                if(pl != null) pl.NetPlayer.score = 0;
            }

            _simulation.Sync();
        }

        public void HandlePlayerMove(int id, int moveDir)
        {
            var player = _worldstate.Players[id];
            var po = _worldstate.GetPlayerObjectForPlayer(player);
            int maxSpeed = player.isSpeedBuffed ? 600 : 300;
            po.Velocity = new Vector2(moveDir > 0 ? Math.Min(po.Velocity.X + 2f + (po.stand ? PlayerConstants.PlayerSpeed : 0), maxSpeed) :
                                                                   Math.Max(po.Velocity.X - 2f - (po.stand ? PlayerConstants.PlayerSpeed : 0), -maxSpeed),
                                                                   po.Velocity.Y);
            if (moveDir > 0)
            {
                player.NetPlayer.flags &= ~PlayerFlags.IsMovingLeft;
                player.FacingRight = true;
            }
            else
            {
                player.NetPlayer.flags |= PlayerFlags.IsMovingLeft;
                player.FacingRight = false;
            }
        }

        //TODO: HandleCollisions shouldn't be the method responsible for running physics.
        public void HandleCollisions(TimeSpan timeSpan)
        {
            //PhysObjects.Where(a => a is Player).ToList().ForEach(a => ((Player)a).PreSimPos = ((Player)a).Position);
            _simulation.Sync();
            _simulation.Simulate((float)timeSpan.TotalMilliseconds / 500.0f);

            var collisions = _simulation.GetCollisions();
            var impermeableCollisions = collisions.GetCollisionsOf(Category.Players, Category.Walls);
            var bulletWallCollisions = collisions.GetCollisionsOf(Category.Bullets, Category.Walls);
            var bulletTargetCollisions = collisions.GetCollisionsOf(Category.Bullets, Category.Targets);
            var pickupPlayerCollisions = collisions.GetCollisionsOf(Category.Pickup, Category.Players);
            var bulletPlayerCollisions = collisions.GetCollisionsOf(Category.Bullets, Category.Players);

            //TODO: Do something.

            //I have to set every player's stand flag as false, otherwise it will stay true before collision with a wall can be checked.            
            var players = _worldstate.GetActivePlayerObjects();
            foreach (var c in players) c.stand = false;

            //Logger.Log(LoggingType.Physics, "Detected {0} player-wall collisions.", impermeableCollisions.Count);

            foreach (var c in impermeableCollisions)
            {
                var po = (PlayerObject)((c.Object1 is PlayerObject) ? c.Object1 : c.Object2);
                var wo = (c.Object1 == po) ? c.Object2 : c.Object1;
                    
                //Logger.Log(LoggingType.Physics, "Player {0} ({1},{2},{3},{4}) collided with wall ({5},{6},{7},{8}).",
                //    po.Name, po.Position.X, po.Position.Y, po.Width, po.Height, wo.Position.X, wo.Position.Y, wo.Width, wo.Height);

                //po.Position = po.PreSimPos;

                //If wall is under player, than he stands, so he can jump.
                if (po.Position.Y + po.Height / 2 <= wo.Position.Y - wo.Height / 2)
                {
                    po.stand = true;
                }
            }

          
            foreach (var c in bulletWallCollisions)
            {
                var bo = ((c.Object1 is Bullet) ? c.Object1 : c.Object2);
                //Logger.Log(LoggingType.Physics, "Bullet collided with a wall");
                _worldstate.RemovePhysObject(bo.Id);
            }


            foreach (var c in bulletTargetCollisions)
            {
                var bo = ((c.Object1 is Bullet) ? c.Object1 : c.Object2);
                var to = (c.Object1 == bo) ? c.Object2 : c.Object1;
                //Logger.Log(LoggingType.Physics, "Bullet collided with a wall");
                HandleTargetHit(to as Target, bo.Id, (bo as Bullet).PlayerId);
            }

            foreach (var c in pickupPlayerCollisions)
            {
                var pickupO = (c.Object1 is Pickup) ? c.Object1 : c.Object2;
                var playerO = (c.Object1 == pickupO) ? c.Object2 : c.Object1;
                HandlePickupCollision(pickupO as Pickup, (playerO as PlayerObject).Id);
            }

            foreach(var c in bulletPlayerCollisions)
            {
                var bullet = ((c.Object1 is Bullet) ? c.Object1 : c.Object2) as Bullet;
                var player = ((c.Object1 == bullet) ? c.Object2 : c.Object1) as PlayerObject;
                HandlePlayerHit(player.Id, bullet.Id);
            }
        }
        

        public void HandlePlayerJump(int id)
        {
            var player = _worldstate.Players[id];
            var po = _worldstate.GetPlayerObjectForPlayer(player);

            if (!po.stand)
                return;

            //Logger.Log(LoggingType.Debug, "Player {0} actually jump", player.Name);
            po.Velocity = new Vector2(po.Velocity.X, -PlayerConstants.PlayerJumpPower);
            player.NetPlayer.flags |= PlayerFlags.Jump;
        }

        public void HandlePlayerShoot(int id)
        {
            var player = _worldstate.Players[id];
            var gun = player.CurrentGun;
            IShootHandler shootHandler;
            switch (gun.Type)
            {
                    case Equipment.Pistol:
                        shootHandler = new PistolShootHandler();
                        break;
                    case Equipment.Shotgun:
                        shootHandler = new ShotgunShootHandler();
                        break;
                    default:
                        shootHandler = new PistolShootHandler();
                        break;
            }
            shootHandler.Shoot(_worldstate, id);
            _simulation.Sync();
        }

        public void HandleTargetHit(Target target, short bulletId, int playerId)
        {
            if (target.alreadyHit) return;
            target.alreadyHit = true;
            _worldstate.Players[playerId].NetPlayer.score += target.score;
            Vector2 newTargetPosition =_worldstate.Map.GetSensinbleTargetPosition();
            Array values = Enum.GetValues(typeof(TargetTypes));
            _worldstate.AddNonplayerPhysObject(new Target(newTargetPosition, (TargetTypes)values.GetValue(new Random().Next(values.Length))));
            _worldstate.Map.CurrentlyAvailableTargetPositions.Add(target.Position);
            _worldstate.Map.CurrentlyAvailableTargetPositions.Remove(newTargetPosition);
            _worldstate.RemovePhysObject(target.Id);
            _worldstate.RemovePhysObject(bulletId);
            _simulation.Sync();
        }

        private void HandlePlayerHit(short playerId, short bulletId)
        {
            var bullet = _worldstate.PhysObjects[bulletId] as Bullet;
            if(bullet == null || bullet.PlayerId == playerId)
            {
                return; // ignore collision if playerShot is the same as playerShooting
            }
            var playerShot = _worldstate.Players[playerId];
            var playerShooting = _worldstate.Players[bullet.PlayerId];
            playerShot.NetPlayer.health -= 10;
            if(playerShot.NetPlayer.health <= 0)
            {
                var newPickups = _worldstate.Map.CreatePickupObjects(new Random().Next(0, 2));
                newPickups.ForEach(_worldstate.AddNonplayerPhysObject);
                _worldstate.GetPlayerObjectForPlayer(playerShot).Position =  _worldstate.Map.GetSensibleSpawnPosition();
                playerShooting.NetPlayer.score += 100; //magic numbers for the win
                playerShot.NetPlayer.health = 100;
                playerShot.isSpeedBuffed = false;
                playerShot.ResetGuns();
            }
            _worldstate.RemovePhysObject(bulletId);
            _simulation.Sync();
        }

        public void HandlePlayerRaiseGun(int id)
        {
            var player = _worldstate.Players[id];
            if(player.NetPlayer.shoot_angle<Math.PI/2)
                player.NetPlayer.shoot_angle += (float)0.03; // magic number
        }

        public void HandlePlayerLowerGun(int id)
        {
            var player = _worldstate.Players[id];
            if(player.NetPlayer.shoot_angle>-Math.PI/2)
                player.NetPlayer.shoot_angle -= (float)0.03; // magic number
        }

        public void HandlePickupCollision(Pickup pickup, int id)
        {
            var player = _worldstate.Players[id];
            pickup.PickupFunction(player);
            _worldstate.RemovePhysObject(pickup.Id);
            _worldstate.Map.CurrentlyAvailablePickupPositions.Add(pickup.Position);
            _simulation.Sync();
        }

    }
}
