﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ServerLogic.ShootingHandlers
{
    interface IShootHandler
    {
        void Shoot(WorldState.Worldstate worldState, int playerId);

    }
}
