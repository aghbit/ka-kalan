﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Physics;
using WorldState;

namespace ServerLogic.ShootingHandlers
{
    class PistolShootHandler : IShootHandler
    {
        public void Shoot(WorldState.Worldstate worldstate, int playerId)
        {
            var player = worldstate.Players[playerId];
            var po = worldstate.GetPlayerObjectForPlayer(player);
            var gun = player.CurrentGun;
            if ((DateTime.Now - player.LastShootTime).Milliseconds <= gun.Cooldown)
                return;

            Vector2 velocity;
            Vector2 force;
            Vector2 bulletPosition = po.Position;
            bulletPosition.Y -= PlayerConstants.PlayerHeight / 5;
            int forceConst = 30;
            if (player.FacingRight)
            {
                velocity = new Vector2(0, 0);
                force = new Vector2(((float)(forceConst * Math.Cos(player.NetPlayer.shoot_angle))), -(float)(forceConst * Math.Sin(player.NetPlayer.shoot_angle)));
                bulletPosition.X += PlayerConstants.PlayerWidth / 2 + 1;
            }
            else
            {
                velocity = new Vector2(0, 0);
                force = new Vector2(-((float)(forceConst * Math.Cos(player.NetPlayer.shoot_angle))), -(float)(forceConst * Math.Sin(player.NetPlayer.shoot_angle)));
                bulletPosition.X -= PlayerConstants.PlayerWidth / 2 + 1;
            }
            // magic number: Width, Height, mass
            Bullet bullet = new Bullet(bulletPosition, velocity, force, 1, 1, 0.001f, Category.Bullets, playerId);
            worldstate.AddNonplayerPhysObject(bullet);
            player.LastShootTime = DateTime.Now;
        }
    }
}
