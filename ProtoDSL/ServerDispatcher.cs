using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Your.Namespace
{
	public class YourDispatcher
	{
		public YourDispatcher()
		{
			throw new NotImplementedException("Fill me in :D");
		}
	
		public bool DispatchPacket(NetworkMessage msg)
		{
			switch (msg.PacketType)
			{
				case PacketType.HelloPacket:
					return ProcessHelloPacket(msg, msg.Packet as HelloPacket);
				case PacketType.BaseGamestatePacket:
					return ProcessBaseGamestatePacket(msg, msg.Packet as BaseGamestatePacket);
				case PacketType.UpdateGamestatePacket:
					return ProcessUpdateGamestatePacket(msg, msg.Packet as UpdateGamestatePacket);
				case PacketType.RejectPacket:
					return ProcessRejectPacket(msg, msg.Packet as RejectPacket);
				case PacketType.EndGamePacket:
					return ProcessEndGamePacket(msg, msg.Packet as EndGamePacket);
				case PacketType.PlayerUpdatePacket:
					return ProcessPlayerUpdatePacket(msg, msg.Packet as PlayerUpdatePacket);
			}
		
			return false;
		}
		
		private bool ProcessHelloPacket(NetworkMessage msg, HelloPacket packet)
		{
			return false;
		}
		
		private bool ProcessBaseGamestatePacket(NetworkMessage msg, BaseGamestatePacket packet)
		{
			return false;
		}
		
		private bool ProcessUpdateGamestatePacket(NetworkMessage msg, UpdateGamestatePacket packet)
		{
			return false;
		}
		
		private bool ProcessRejectPacket(NetworkMessage msg, RejectPacket packet)
		{
			return false;
		}
		
		private bool ProcessEndGamePacket(NetworkMessage msg, EndGamePacket packet)
		{
			return false;
		}
		
		private bool ProcessPlayerUpdatePacket(NetworkMessage msg, PlayerUpdatePacket packet)
		{
			return false;
		}
		
	}
	
}
