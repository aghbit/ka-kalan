﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProtoDSL
{
    internal enum SymbolType
    {
        Struct = 0,
        Enum = 1,
        Typedef
    }

    internal class Symbol
    {
        public string Name;
        public SymbolType Type;
        public bool NoGen;
    };

    internal class Spec
    {
        public string Name;
        public string Type;
        public string ClistType;
        public string ClistEncode;
    };

    internal class Struct : Symbol
    {
        public Struct()
        {
            Type = SymbolType.Struct;
        }

        public List<Tuple<Spec, string>> Members;
    }

    internal class Typedef : Symbol
    {
        public Typedef()
        {
            Type = SymbolType.Typedef;
        }

        public string MappedTo;

    }

    internal class Enum : Symbol
    {
        public Enum()
        {
            Type = SymbolType.Enum;
        }

        public List<string> Body;
    }

    internal enum ParserState
    {
        BetweenSymbols,
        WaitForOpenEnum,
        WaitForOpenStruct,
        ParsingEnum,
        ParsingStruct
    };

    public class Parser
    {
        private string[] _lines;
        private List<Symbol> _symbols;
        private Dictionary<string, Symbol> _st;
        private Dictionary<string, string> _sfunc; 
        private Dictionary<string, string> _sread;
        private Dictionary<string, int> _sreadLength;

        public Parser(string[] lines)
        {
            _lines = lines;
            _symbols = new List<Symbol>();
            _st = new Dictionary<string, Symbol>();
            _sfunc = new Dictionary<string, string>();
            _sread = new Dictionary<string, string>();
            _sreadLength = new Dictionary<string, int>();
            Parse();
            InitSymbols();
            AnalyzeSymbolTable();
        }

        private void InitSymbols()
        {
            _st["short"] = null;
            _st["int"] = null;
            _st["double"] = null;
            _st["float"] = null;
            _st["byte"] = null;
            _st["char"] = null;

            
            _sfunc["short"] = "BitConverter.GetBytes({0})";
            _sfunc["int"] = "BitConverter.GetBytes({0})";
            _sfunc["double"] = "BitConverter.GetBytes({0})";
            _sfunc["float"] = "BitConverter.GetBytes({0})";
            _sfunc["byte"] = "{0}";
            _sfunc["char"] = "{0}";

            _sread["short"] = "BitConverter.ToInt16(data.Array, data.Offset)";
            _sread["int"] = "BitConverter.ToInt32(data.Array, data.Offset)";
            _sread["double"] = "BitConverter.ToDouble(data.Array, data.Offset)";
            _sread["float"] = "BitConverter.ToSingle(data.Array, data.Offset)";
            _sread["byte"] = "data.Array[data.Offset]";
            _sread["char"] = "(char) data.Array[data.Offset]";

            _sreadLength["short"] = 2;
            _sreadLength["int"] = 4;
            _sreadLength["double"] = 8;
            _sreadLength["float"] = 4;
            _sreadLength["byte"] = 1;
            _sreadLength["char"] = 1;

            _symbols.Add(new Enum() { Body = new List<string>(), Name = "PacketType", Type = SymbolType.Enum, NoGen = true});
            _st["PacketType"] = _symbols.Last();

        }

        private void AnalyzeSymbolTable()
        {
            var unres = _symbols.Where(a => a.Type == SymbolType.Struct).
                Select(b => b as Struct).
                SelectMany(c => c.Members.Select(d => d.Item1.Type.StartsWith("clist") ? d.Item1.ClistType : d.Item1.Type)).
                Union(_symbols.Where(e => e.Type == SymbolType.Typedef).Select(f => (f as Typedef).MappedTo)).
                Except(_st.Keys).
                ToList();

            foreach (var k in unres)
            {
                throw new Exception("Unresolved symbol " + k);
            }
        }

        void Parse()
        {
            ParserState ps = ParserState.BetweenSymbols;
            var cur = new Symbol();

            foreach (var l in _lines.Where(a => !String.IsNullOrWhiteSpace(a) && !a.StartsWith("//")))
            {
                var tl = l.Trim();

                switch (ps)
                {
                    case ParserState.BetweenSymbols:
                        var sd = tl.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                        if (sd[0] == "enum")
                        {
                            cur = new Enum() {Name = sd[1], Body = new List<string>()};
                            ps = ParserState.WaitForOpenEnum;
                        }
                        else if (sd[0] == "struct")
                        {
                            cur = new Struct() {Name = sd[1], Members = new List<Tuple<Spec, string>>()};
                            ps = ParserState.WaitForOpenStruct;
                        }
                        else if (sd[0] == "typedef")
                        {
                            if (sd.Count() != 3)
                                throw new Exception("Invalid typedef");
                            cur = new Typedef() {MappedTo = sd[2], Name = sd[1]};
                            FinalizeSymbol(cur);
                        }
                        else
                            throw new Exception("Expected enum or struct");
                        break;

                    case ParserState.WaitForOpenEnum:
                        if (tl != "{")
                            throw new Exception("Expected {");
                        ps = ParserState.ParsingEnum;
                        break;
                    case ParserState.WaitForOpenStruct:
                        if (tl != "{")
                            throw new Exception("Expected {");
                        ps = ParserState.ParsingStruct;
                        break;
                    case ParserState.ParsingEnum:
                        if (tl == "}")
                        {
                            FinalizeSymbol(cur);
                            ps = ParserState.BetweenSymbols;
                        }
                        else
                        {
                            (cur as Enum).Body.Add(tl);
                        }
                        break;
                    case ParserState.ParsingStruct:
                        if (tl == "}")
                        {
                            FinalizeSymbol(cur);
                            ps = ParserState.BetweenSymbols;
                        }
                        else
                        {
                            const string pattern = @"((clist<(\w+)(\s)*(,\s(\w+))?>)|\w+)(\s+)(\w+);";
                            var matches = Regex.Matches(tl, pattern, RegexOptions.IgnoreCase);

                            if (matches.Count != 1)
                                throw new Exception("Regex failed to match on " + tl);

                            var m = matches[0];
                            var s = (cur as Struct);

                            s.Members.Add(new Tuple<Spec, string>(new Spec() {
                                ClistEncode = m.Groups[6].Value,
                                ClistType = m.Groups[3].Value,
                                Name = m.Groups[8].Value,
                                Type = m.Groups[1].Value}, m.Groups[8].Value));
                            //1,3,6,8
                        }
                        break;
                }
            }


            if (ps != ParserState.BetweenSymbols)
                throw new Exception("Invalid state at end: " + ps);
        }

        private void FinalizeSymbol(Symbol cur)
        {
            if (_st.ContainsKey(cur.Name))
                throw new Exception("Duplicate type definiton " + cur.Name);
            _st[cur.Name] = cur;
            _symbols.Add(cur);

        }

        public IEnumerable<string> GenerateProtoCode(out List<string> dispatcher, out List<string> senderClient, out List<string> senderServer)
        {
            var res = new List<string> {
                "using System;",
                "using System.Collections.Generic;",
                "using System.Linq;",
                "using System.IO;",
                "",
                "namespace Protocol",
                "{"};

            var packetTypes = _symbols.Where(a => a.Type == SymbolType.Struct && a.Name.EndsWith("Packet"));
            res.Add("\tpublic enum PacketType");
            res.Add("\t{");
            var ptL = packetTypes.Select(d => "\t\t" + d.Name + ",").ToList();
            ptL[ptL.Count - 1] = ptL.Last().Substring(0, ptL.Last().Length - 1);
            res.AddRange(ptL);
            res.Add("\t}");
            res.Add("\t");

            GenerateDispatchSend(out senderClient, packetTypes, false);
            GenerateDispatchSend(out senderServer, packetTypes, true);

            foreach (var c in _symbols.Where(a => !a.NoGen))
            {
                if (c.Type == SymbolType.Enum)
                {
                    if (c.Name.EndsWith("Flags"))
                        res.Add("\t[Flags]");
                    res.Add("\tpublic enum " + c.Name);
                    res.Add("\t{");
                    res.AddRange((c as Enum).Body.Select(d => "\t\t" + d));
                    res.Add("\t}");
                }
                else if (c.Type == SymbolType.Struct)
                {
                    res.Add("\tpublic partial class " + c.Name);
                    res.Add("\t{");
                    foreach (var m in (c as Struct).Members)
                    {
                        if (String.IsNullOrWhiteSpace(m.Item1.ClistType))
                            res.Add(String.Format("\t\tpublic {0} {1} {{get; set;}}", m.Item1.Type, m.Item1.Name));
                        else
                            res.Add(String.Format("\t\tpublic List<{0}> {1} {{get; set;}}", m.Item1.ClistType, m.Item1.Name));
                        
                    }

                    res.Add("\t}");
                    //res.Add("\t");
                }

                res.Add("");
            }

            res.AddRange(GenerateWriteSerializers());
            res.AddRange(GenerateReadSerializers());

            res.Add("}");

            GenerateDispatcher(out dispatcher, packetTypes);

            return res;
        }

        private static void GenerateDispatchSend(out List<string> sender, IEnumerable<Symbol> packetTypes, bool server=true)
        {
            sender = new List<string>();
            foreach (var pt in packetTypes)
            {
                if (server)
                    sender.Add(String.Format("\t\tpublic void SendPacket({0} packet, int channel, SendOptions options, NetEndpointId target)", pt.Name));
                else
                    sender.Add(String.Format("\t\tpublic void SendPacket({0} packet, int channel, SendOptions options)", pt.Name));

                sender.Add("\t\t{");
                sender.Add("\t\t\tvar bytes = new List<byte>();");
                sender.Add(String.Format("\t\t\tvar gph = new GamePacketHeader() {{type = PacketType.{0}}};", pt.Name));
                sender.Add("\t\t\tvar sendBytes = packet.GetPDLBytes();");
                sender.Add("\t\t\tgph.length = (short) sendBytes.Length;");
                sender.Add("\t\t\tbytes.AddRange(gph.GetPDLBytes());");
                sender.Add("\t\t\tbytes.AddRange(sendBytes);");
                if (server)
                    sender.Add("\t\t\tSendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});");
                else
                    sender.Add("\t\t\tSendData(bytes.ToArray(), channel, options);");
                    
                sender.Add("\t\t}");
                sender.Add("\t\t");
            }

            //DispatchIncomingPacket(gph, packetBytes, out networkMessage.PacketType)

            sender.Add("\t\tpublic Object DispatchIncomingPacket(GamePacketHeader gph, byte[] packetBytes, out PacketType pt)");
            sender.Add("\t\t{");
            sender.Add("\t\t\tpt = gph.type;");
            sender.Add("\t\t\tvar ars = new ArraySegment<byte>(packetBytes);");
            sender.Add("\t\t\tswitch (gph.type)");
            sender.Add("\t\t\t{");

            sender.AddRange(
                packetTypes.Select(pt => String.Format("\t\t\t\tcase PacketType.{0}:\r\n\t\t\t\t\treturn new {0}(ref ars);", pt.Name)));

            sender.Add("\t\t\t}");
            sender.Add("\t\t\t");
            sender.Add("\t\t\treturn null;");
            sender.Add("\t\t}");
            sender.Add("\t\t");
        }

        private static void GenerateDispatcher(out List<string> dispatcher, IEnumerable<Symbol> packetTypes)
        {
            dispatcher = new List<string>
            {
                "using System;",
                "using System.Collections.Generic;",
                "using System.Linq;",
                "using System.IO;",
                "",
                "namespace Your.Namespace",
                "{",
                "\tpublic class YourDispatcher",
                "\t{",
                "\t\tpublic YourDispatcher()",
                "\t\t{",
                "\t\t\tthrow new NotImplementedException(\"Fill me in :D\");",
                "\t\t}",
                "\t",
                "\t\tpublic bool DispatchPacket(NetworkMessage msg)",
                "\t\t{",
                "\t\t\tswitch (msg.PacketType)",
                "\t\t\t{"
            };

            foreach (var ptype in packetTypes)
            {
                dispatcher.Add("\t\t\t\tcase PacketType." + ptype.Name + ":");
                dispatcher.Add(String.Format("\t\t\t\t\treturn Process{0}(msg, msg.Packet as {0});", ptype.Name));
            }

            
            dispatcher.Add("\t\t\t}");
            dispatcher.Add("\t\t");
            dispatcher.Add("\t\t\treturn false;");
            dispatcher.Add("\t\t}");
            dispatcher.Add("\t\t");

            foreach (var ptype in packetTypes)
            {
                dispatcher.Add(String.Format("\t\tprivate bool Process{0}(NetworkMessage msg, {0} packet)", ptype.Name));
                dispatcher.Add("\t\t{");
                dispatcher.Add("\t\t\treturn false;");
                dispatcher.Add("\t\t}");
                dispatcher.Add("\t\t");
             }

            dispatcher.Add("\t}");
            dispatcher.Add("\t");
            dispatcher.Add("}");
        }

        private List<string> GenerateWriteMethod(string _type)
        {
            var myOut = new List<string>();
            var prepend = new List<string>();
            var type = _st[_type];
            if (type.Type == SymbolType.Enum)
            {
                _sfunc[_type] = "(byte) " + _sfunc["byte"];
                return new List<string>();
            }
            else if (type.Type == SymbolType.Struct)
            {
                if (!_sfunc.ContainsKey(_type))
                {
                    _sfunc[_type] = "GetPDLBytes({0})";
                }

                myOut.Add("\t\tpublic static byte[] GetPDLBytes(this " + _type + " src)");
                myOut.Add("\t\t{");
                myOut.Add("\t\t\tvar ms = new MemoryStream();");
                myOut.Add("\t\t\tvar bw = new BinaryWriter(ms);");

                foreach (var k in (type as Struct).Members)
                {

                    if (String.IsNullOrWhiteSpace(k.Item1.ClistType))
                    {
                        if (!_sfunc.ContainsKey(k.Item1.Type))
                            prepend.AddRange(GenerateWriteMethod(k.Item1.Type));

                        myOut.Add(String.Format(String.Format("\t\t\tbw.Write({0});", _sfunc[k.Item1.Type]), "src." + k.Item1.Name));
                    }
                    else
                    {
                        if (!_sfunc.ContainsKey(k.Item1.ClistType))
                            prepend.AddRange(GenerateWriteMethod(k.Item1.ClistType));

                        var clistEncode = !String.IsNullOrWhiteSpace(k.Item1.ClistEncode) ? k.Item1.ClistEncode : "byte";
                        myOut.Add(String.Format(String.Format("\t\t\tbw.Write({0});", _sfunc[clistEncode]), "(" + clistEncode +  ") src." + k.Item1.Name + ".Count()"));
                        myOut.Add(String.Format("\t\t\tsrc.{0}.ForEach(a => bw.Write({1}));", k.Item1.Name,
                            String.Format(_sfunc[k.Item1.ClistType], "a")));
                    }
                }

                myOut.Add("\t\t\treturn ms.ToArray();");
                myOut.Add("\t\t}");
                myOut.Add("\t\t");
            }

            prepend.AddRange(myOut);
            return prepend;
        }

        private List<string> GenerateWriteSerializers()
        {
            var myOut = new List<string>();
            myOut.Add("\tpublic static class WritePDLExtensions");
            myOut.Add("\t{");
            myOut.Add("\t\t");
            foreach (var k in _symbols.Where(a => a.Type == SymbolType.Struct))
            {
                if (_sfunc.ContainsKey(k.Name))
                    continue;

                myOut.AddRange(GenerateWriteMethod(k.Name));
            }
            myOut.Add("\t}");

            return myOut;
        }

        private List<string> GenerateReadSerializers()
        {
            var myOut = new List<string>();
            foreach (var c in _symbols.Where(a => a.Type == SymbolType.Struct))
            {
                myOut.Add("\tpublic partial class " + c.Name);
                myOut.Add("\t{");
                myOut.Add("\t\t");

                myOut.Add("\t\tpublic " + c.Name + "()");
                myOut.Add("\t\t{");
                myOut.Add("\t\t}");

                myOut.Add("\t\tpublic " + c.Name + "(ref ArraySegment<byte> data)");
                myOut.Add("\t\t{");



                foreach (var k in (c as Struct).Members)
                {
                    if (!String.IsNullOrWhiteSpace(k.Item1.ClistType))
                    {
                        string fullCtype = "byte";
                        if (!String.IsNullOrWhiteSpace(k.Item1.ClistEncode))
                            fullCtype = "short";

                        ProcessUnknownReadSymbol(k.Item1.ClistType);

                        myOut.Add("\t\t\tvar len_" + k.Item1.Name + " = " + _sread[fullCtype] + ";");
                        myOut.Add(
                            String.Format(
                                "\t\t\tdata = new ArraySegment<byte>(data.Array, data.Offset + {0}, data.Count - {0});",
                                +_sreadLength[fullCtype]));

                        myOut.Add(String.Format("\t\t\t{0} = new List<{1}>();", k.Item1.Name, k.Item1.ClistType));
                        myOut.Add("\t\t\tfor (int i = 0; i < len_" + k.Item1.Name + "; i++)");
                        myOut.Add("\t\t\t{");
                        myOut.Add(String.Format("\t\t\t\t{0}.Add({1});", k.Item1.Name, _sread[k.Item1.ClistType]));
                        if (_st[k.Item1.ClistType] == null || _st[k.Item1.ClistType].Type == SymbolType.Enum)
                            myOut.Add(
                                String.Format(
                                    "\t\t\t\tdata = new ArraySegment<byte>(data.Array, data.Offset + {0}, data.Count - {0});",
                                    +_sreadLength[k.Item1.ClistType]));

                        myOut.Add("\t\t\t}");
                        continue;
                    }


                    ProcessUnknownReadSymbol(k.Item1.Type);
                    myOut.Add(String.Format("\t\t\t{0} = {1};", k.Item1.Name, _sread[k.Item1.Type]));

                    if (_st[k.Item1.Type] == null || _st[k.Item1.Type].Type == SymbolType.Enum)
                        myOut.Add(
                            String.Format(
                                "\t\t\tdata = new ArraySegment<byte>(data.Array, data.Offset + {0}, data.Count - {0});",
                                +_sreadLength[k.Item1.Type]));

                    
                }

                myOut.Add("\t\t}");
                myOut.Add("\t}");
            }


            return myOut;
        }

        private void ProcessUnknownReadSymbol(string k)
        {
            if (_sread.ContainsKey(k))
                return;
            if (_st[k] == null)
                return;

            if (_st[k].Type == SymbolType.Struct)
                _sread[k] = "new " + k + "(ref data)";
            else if (_st[k].Type == SymbolType.Enum)
            {
                _sread[k] = "(" + k + ") " + _sread["byte"];
                _sreadLength[k] = _sreadLength["byte"];
            }
        }
    }
} 
