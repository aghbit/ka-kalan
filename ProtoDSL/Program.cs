﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ProtoDSL
{
    class Program
    {
        static IEnumerable<string> GenerateNetCode(string cls, string superclass, List<string> dispatchers)
        {
            yield return "using System;";
            yield return "using System.Collections.Generic;";
            yield return "using Net.Net;";
            yield return "using Lidgren.Network;";
            yield return "using Net.Net.Types;";
            yield return "using Protocol;";
            yield return "";
            yield return "namespace Kalan.Net";
            yield return "{";
            yield return String.Format("\tpublic partial class {0} : {1}", cls, superclass);
            yield return "\t{";
            foreach (var d in dispatchers)
                yield return d;
            yield return "\t}";
            yield return "}";
        }

        static void Main(string[] args)
        {
            var input = args[0];
            var parser = new Parser(File.ReadAllLines(input));
            List<string> dispatchers, senderCli, senderSv;
            var outlines = parser.GenerateProtoCode(out dispatchers, out senderCli, out senderSv);

            File.WriteAllLines(args[1], outlines);
            File.WriteAllLines(args[2], dispatchers);
            File.WriteAllLines(args[3], dispatchers);
            File.WriteAllLines(args[4], GenerateNetCode("NetworkServer", "AbstractServer", senderSv));
            File.WriteAllLines(args[5], GenerateNetCode("NetworkClient", "AbstractClient", senderCli));


            MemoryStream str = new MemoryStream(128);
            BinaryWriter bw = new BinaryWriter(str);
        }
    }
}
