using System;
using System.Collections.Generic;
using System.IO;
using Kalan.Net;
using KalanClient.API;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UI;
using PlayerController;
using UI.UIComponents;
using UI.WorldObjects;
using Model = UI.UIComponents.Model;

namespace KalanClient
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class ClientGame : Game
    {
        #region ui-dependencies
        private InputHandler _inputHandler;
        private WorldDrawer _worldDrawer;
        private DrawerProperties _drawerProperties;
        private Page _startPage;
        private GraphicsDeviceManager _deviceManager;
        private SpriteBatch _spriteBatch;
        private ConvertUnit _convertUnit;
        private IUIDrawer _uiDrawer;
        #endregion

        private bool _isRunning;
        private bool _timeOut;

        private IClientApi _clientApi;

        public ClientGame()
        {
            string name = new Random().Next().ToString(); //Somewhere, in another dimension, this is properly settable. Not in this one though.
            _clientApi = new ClientApi(name);

            _deviceManager = new GraphicsDeviceManager(this);
            _drawerProperties = new DrawerProperties();
            _convertUnit = new ConvertUnit();
            _startPage = new MainPage();

            ContentLoaderRetriever.Initialize(Content);

            _isRunning = false;
            Content.RootDirectory = "Content";
        }

        public void StartGame()
        {
            _clientApi.Connect(NetConfigConstants.HOST_IP, NetConfigConstants.PORT);
            _clientApi.Model.Timer.Start();
            _timeOut = false;
            _isRunning = true;
            _startPage.Disable();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _worldDrawer = new WorldDrawer(_spriteBatch);
            _uiDrawer = new UIDrawer(_spriteBatch);

            //_clientApi.Connect(NetConfigConstants.LOOPBACK_IP, NetConfigConstants.PORT);

            _inputHandler = new InputHandler(new KeyActionMap());

            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // everything in here is supposed to act as a test for new features
            // RE-CHECK BEFORE RELEASE
            Player player = new Player("char", new Vector2(50, 50), 0, true);
            player.LoadContent();
            _clientApi.World.Players.Add(player);
            
            const int timerWidth = 50;
            const int timerHeight = 20;
            TimeSpan timerDuration = new TimeSpan(0,1,0);
            const int scoreX = 20;
            const int scoreY = 10;

            _clientApi.Model.Timer = new Timer(timerDuration, new Vector2(GraphicsDevice.Viewport.Width - timerWidth, timerHeight));
            _clientApi.Model.Timer.LoadContent();
            _clientApi.Model.Score = new Score(new Vector2(scoreX, scoreY));
            _clientApi.Model.Score.LoadContent();

            Background background = new Background("background");
            background.LoadContent();
            _clientApi.World.Background = background;

            _convertUnit.WinSize = new Vector2(_deviceManager.GraphicsDevice.Viewport.Width, _deviceManager.GraphicsDevice.Viewport.Height);

            // Prepare GUI resources and initialize the start page
            SpriteFont greySpriteFont = Content.Load<SpriteFont>(@"Resources/Texture");
            Texture2D greyImageMap = Content.Load<Texture2D>(@"Resources/ImageMap");
            string greyMap = File.OpenText(@"Content/Resources/Map.txt").ReadToEnd();

            _startPage.Init(this, greyImageMap, greyMap, greySpriteFont);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            _clientApi.Disconnect();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                Exit();
            }

            if (!_isRunning)
            {
                _startPage.Update(gameTime);
            }
            else
            {
                // handle input and send client changes to the server
                IEnumerable<InputAction> actions = _inputHandler.HandleInput();
                _clientApi.PerformActions(actions);

                _clientApi.UpdateWorld();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (!_isRunning)
            {
                _startPage.Draw();
            }
            else if (_timeOut)
            {
                _spriteBatch.Begin();
                _uiDrawer.DrawUI(_clientApi.Model, _convertUnit);
                _spriteBatch.End();
            }
            else
            {
                if (_clientApi.Model.Timer.TimeOut())
                {
                    _timeOut = true;
                    _clientApi.Model.Score.Position = new Vector2(GraphicsDevice.Viewport.Width/2, GraphicsDevice.Viewport.Height / 2) - _clientApi.Model.Score.TextSize()/2;
                    _clientApi.Model.Timer.Position = new Vector2(-200, -200);
                }
                _convertUnit.MapSize = _clientApi.World.MapSize;
                _convertUnit.Mid = _clientApi.World.Players[0].Position;
                _convertUnit.SetMode(ConvertMode.FitMapToScreen);
                _worldDrawer.DrawWorld(_clientApi.World, _drawerProperties, _convertUnit);
                _spriteBatch.Begin();
                _uiDrawer.DrawUI(_clientApi.Model, _convertUnit);
                _spriteBatch.End();
            }

            base.Draw(gameTime);
        }
    }
}
