﻿using KalanClient.ClientPsychics.Events;
using KalanClient.ClientPsychics.PhysicsObjects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KalanClient.ClientPsychics
{
    public static class Simulator
    {
        private const float MAX_FRAME_TIME = 0.04f;

        public static PhysicsEventCollection Simulate(IWorld world) 
        {
            PhysicsEventCollection pec = new PhysicsEventCollection();
            float secondsSinceLastSimulation = world.MillisecondsSinceLastSimulation/1000.0f;
            int loops = (int)Math.Floor(secondsSinceLastSimulation/MAX_FRAME_TIME);
            float secondsRemainder = secondsSinceLastSimulation % MAX_FRAME_TIME;
            ICollection<IPhysicsObject> phobjects = world.PhysicsObjects;
            world.MillisecondsSinceLastSimulation = 0;
            for (int i = 0; i < loops; ++i)
            {
                SimulateInternal(phobjects, pec, MAX_FRAME_TIME);
            }

            SimulateInternal(phobjects, pec, secondsRemainder);
            return pec;
        }

        private static PhysicsEventCollection SimulateInternal(
            ICollection<IPhysicsObject> phobjects, 
            PhysicsEventCollection pec, 
            float secondsSinceLastSimulation)
        {
            List<IPhysicsObject> phobjectsNotRepeated = phobjects.ToList();
            foreach (IPhysicsObject phobject in phobjects)
            {
                phobject.Simulate(secondsSinceLastSimulation);
            }

            foreach (IPhysicsObject phobject1 in phobjects)
            {
                phobjectsNotRepeated.Remove(phobject1);
                foreach (IPhysicsObject phobject2 in phobjectsNotRepeated)
                {
                    CheckCollision(pec, phobject1, phobject2, secondsSinceLastSimulation);
                }
            }

            return pec;
        }

        private static void CheckCollision(
            PhysicsEventCollection pec,     
            IPhysicsObject phobject1,       
            IPhysicsObject phobject2,       
            float secondsSinceLastSimulation)
        {
            CollisionType collisionType = phobject2.CheckCollision(phobject1);
            if (collisionType != CollisionType.None)
            {
                CollisionEvent phevent = new CollisionEvent(phobject1, phobject2, collisionType);
                if (!pec.PhysicsEvents.Any(
                    e => e.InteractingObjects.Item1 == phevent.InteractingObjects.Item1
                    && e.InteractingObjects.Item2 == phevent.InteractingObjects.Item2))
                {
                    pec.PhysicsEvents.Add(phevent);
                }

                phobject1.ApplyCollisionCorrection(phevent, secondsSinceLastSimulation);
                phobject2.ApplyCollisionCorrection(phevent, secondsSinceLastSimulation);
            }
        }
    }
}
