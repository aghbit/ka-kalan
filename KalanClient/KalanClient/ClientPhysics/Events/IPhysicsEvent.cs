﻿using System;
using KalanClient.ClientPsychics.PhysicsObjects;
using Microsoft.Xna.Framework;

namespace KalanClient.ClientPsychics.Events
{
    public interface IPhysicsEvent
    {
        Tuple<IPhysicsObject, IPhysicsObject> InteractingObjects { get; }
        Vector2 Position { get; }
    }
}
