﻿using System;
using KalanClient.ClientPsychics.PhysicsObjects;
using Microsoft.Xna.Framework;

namespace KalanClient.ClientPsychics.Events
{
    public class CollisionEvent : IPhysicsEvent
    {
        public CollisionType Type { get; private set; }
        public Vector2 Position { get; private set; }
        public Tuple<IPhysicsObject, IPhysicsObject> InteractingObjects { get; private set; }

        public CollisionEvent(
            IPhysicsObject interactingPhobject1, 
            IPhysicsObject interactingPhobject2,
            CollisionType collisionType)
        {
            if (collisionType == CollisionType.None)
            {
                throw new ArgumentException("CollisionType.None is not allowed");
            }

            InteractingObjects = Tuple.Create(interactingPhobject1, interactingPhobject2);
            Type = collisionType;

            Position = interactingPhobject1.GetCollisionMiddle(interactingPhobject2);
        }
    }
}
