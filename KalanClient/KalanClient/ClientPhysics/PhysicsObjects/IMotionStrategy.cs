﻿namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public interface IMotionStrategy
    {
        void Update(IPhysicsObject phobject, float secondsSinceLastUpdate);
    }
}
