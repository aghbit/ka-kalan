﻿using System;
using Microsoft.Xna.Framework;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public class UniformlyAcceleratedMotionStrategy : IMotionStrategy
    {
        public virtual void Update(IPhysicsObject phobject, float secondsSinceLastUpdate)
        {
            if (secondsSinceLastUpdate < 0.0f)
            {
                throw new ArgumentException("Seconds can't be negative!");
            }

            Vector2 displacementUniform = phobject.Velocity*secondsSinceLastUpdate;
            Vector2 displacementAccelerated =
                phobject.Acceleration*(float)Math.Pow(secondsSinceLastUpdate, 2)*0.5f;
            Vector2 displacement = displacementUniform + displacementAccelerated;

            phobject.Velocity += phobject.Acceleration*secondsSinceLastUpdate;
            phobject.Position = phobject.Position + displacement;
        }
    }
}
