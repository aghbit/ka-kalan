﻿using System.Collections.Generic;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public class SimpleWorld : IWorld
    {
        public ICollection<IPhysicsObject> PhysicsObjects { get; set; }

        public SimpleWorld()
        {
            this.PhysicsObjects = new List<IPhysicsObject>();
        }

        public int MillisecondsSinceLastSimulation { get; set; }
    }
}
