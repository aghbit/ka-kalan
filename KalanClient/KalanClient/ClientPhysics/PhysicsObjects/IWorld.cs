﻿using System.Collections.Generic;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public interface IWorld
    {
        ICollection<IPhysicsObject> PhysicsObjects { get; }

        int MillisecondsSinceLastSimulation { get; set; }
    }
}
