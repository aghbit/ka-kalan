﻿using System.Collections.Generic;
using KalanClient.ClientPsychics.Events;
using Microsoft.Xna.Framework;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public interface IPhysicsObject
    {
        Vector2 Position { get; set; }
        Vector2 Velocity { get; set; }
        Vector2 Acceleration { get; set; }
        ISet<CollisionType> CollisionTypes { get; set; }
        bool CheckInside(Vector2 point);

        CollisionType CheckCollision(IPhysicsObject phobject);

        void Simulate(float secondsSinceLastSimulation);
        void ApplyCollisionCorrection(
            CollisionEvent collisionEvent, 
            float secondsSinceLastSimulation);

        float Mass { get; set; }

        Vector2 GetCollisionMiddle(IPhysicsObject interactingPhobject2);
    }
}
