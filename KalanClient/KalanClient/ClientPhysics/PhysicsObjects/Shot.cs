﻿using KalanClient.ClientPsychics.Events;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public class Shot : IPhysicsObject
    {
        const double MAX_DIST = 1.0;

        private double angleInRadians;
        private IPhysicsObject shooter;

        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public Vector2 Acceleration { get; set; }
        public ISet<CollisionType> CollisionTypes { get; set; }
        public float Mass { get; set; }

        public Shot(
            Vector2 position, 
            double angleInRadians, 
            CollisionType collisionType,
            IPhysicsObject shooter)
        {
            this.Position = position;
            this.angleInRadians = angleInRadians;
            this.CollisionTypes = new HashSet<CollisionType>();
            this.CollisionTypes.Add(collisionType);
            this.shooter = shooter;
        }

        public bool CheckInside(Vector2 point)
        {
            double a = Math.Tan(angleInRadians);
            double b = -1.0;
            double c = Position.Y - (a*Position.X);

            return Math.Abs(a*point.X + b*point.Y + c)/Math.Sqrt(a*a + b*b) < MAX_DIST;
        }

        public CollisionType CheckCollision(IPhysicsObject phobject)
        {
            IEnumerable<CollisionType> intersection = 
                phobject.CollisionTypes.Intersect(this.CollisionTypes);

            if (intersection.Count() == 0 || phobject is Shot || phobject == shooter)
            {
                return CollisionType.None;
            }

            return phobject.CheckCollision(this);
        }

        public void ApplyCollisionCorrection(CollisionEvent collisionEvent, float secondsSinceLastSimulation) 
        {
            //It really should do nothing!
            return;
        }

        public void Simulate(float secondsSinceLastSimulation)
        {
            //It really should do nothing!
            return;
        }

        public Vector2 GetCollisionMiddle(IPhysicsObject phobject)
        {
            return phobject.GetCollisionMiddle(this);
        }
    }
}
