﻿using System;
using System.Collections.Generic;
using System.Linq;
using KalanClient.ClientPsychics.Events;
using Microsoft.Xna.Framework;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public class Box : IPhysicsObject
    {
        #region Members
        private const float COLLISION_CORRECTION_PRECISION = 0.0001f;
        private IMotionStrategy motionStrategy;
        private Vector2 positionBackup;
        private Vector2 accelerationBackup;
        private Vector2 velocityBackup;

        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public Vector2 Acceleration { get; set; }
        public ISet<CollisionType> CollisionTypes { get; set; }
        public float Mass { get; set; }

        public Vector2 Dimensions { get; set; }
        public float LowerBound
        {
            get
            {
                return Position.Y - Dimensions.Y/2;
            }
        }
        public float UpperBound
        {
            get
            {
                return Position.Y + Dimensions.Y/2;
            }
        }
        public float LeftBound
        {
            get
            {
                return Position.X - Dimensions.X/2;
            }
        }
        public float RightBound
        {
            get
            {
                return Position.X + Dimensions.X/2;
            }
        }
        #endregion

        #region Constructors
        private Box()
        {
            this.CollisionTypes = new HashSet<CollisionType>();
        }

        public Box(IMotionStrategy motionStrategy) : 
            this()
        {
            this.motionStrategy = motionStrategy;
        }
        #endregion

        #region Public methods
        public bool CheckInside(Vector2 point)
        {
            // checking without borders
            return point.X > LeftBound && point.X < RightBound && point.Y < UpperBound && point.Y > LowerBound;     
        }

        public CollisionType CheckCollision(IPhysicsObject phobject)
        {
            if (this == phobject)
            {
                return CollisionType.None;
            }

            IEnumerable<CollisionType> intersection = this.CollisionTypes.Intersect(phobject.CollisionTypes);
            CollisionType type = intersection.FirstOrDefault() ?? CollisionType.None;
            if (type != CollisionType.None)
            {
                bool foundCollision = false;

                /* 
                 * I'm aware downcasting should be avoided 
                 * but here introducing visitors instead would make it needlessly complicated
                 */
                if (phobject is Box)
                {
                    foundCollision = CheckBoxCollision(phobject as Box);
                }
                else
                {
                    foundCollision = CheckGridCollision(phobject);
                }

                if (!foundCollision)
                {
                    type = CollisionType.None;
                }
            }

            return type;
        }

        public void Simulate(float secondsSinceLastSimulation)
        {
            this.positionBackup = this.Position;
            this.velocityBackup = this.Velocity;
            this.accelerationBackup = this.Acceleration;
            this.motionStrategy.Update(this, secondsSinceLastSimulation);
        }

        public void ApplyCollisionCorrection(CollisionEvent collisionEvent, float secondsSinceLastSimulation)
        {
            /*
             * This is a brute-force algorithm. 
             * Should be optimised if better performance is needed. 
             */
            IPhysicsObject secondParticipant = collisionEvent.InteractingObjects.Item1;
            if (secondParticipant == this) 
            {
                secondParticipant = collisionEvent.InteractingObjects.Item2;
            }

            secondsSinceLastSimulation /= 2;
            float delta = secondsSinceLastSimulation;
            while (delta > COLLISION_CORRECTION_PRECISION)
            {
                delta /= 2;
                if (CheckCollision(secondParticipant) != CollisionType.None)
                {
                    secondsSinceLastSimulation -= delta;
                }
                else
                {
                    secondsSinceLastSimulation += delta;
                }

                this.Position = this.positionBackup;
                this.Velocity = this.velocityBackup;
                this.Acceleration = this.accelerationBackup;
                this.Simulate(secondsSinceLastSimulation);
            }
        }

        public Vector2 GetCollisionMiddle(IPhysicsObject phobject) 
        {
            Box box = phobject as Box;
            if (box != null) 
            {
                Rectangle rect1 = new Rectangle(
                    (int)LeftBound, 
                    (int)LowerBound, 
                    (int)Dimensions.X, 
                    (int)Dimensions.Y);
                Rectangle rect2 = new Rectangle(
                    (int)box.LeftBound, 
                    (int)box.LowerBound, 
                    (int)box.Dimensions.X, 
                    (int)box.Dimensions.Y);

                Rectangle intersect = Rectangle.Intersect(rect1, rect2);
                return new Vector2(intersect.Center.X, intersect.Center.Y);
            }

            return Position;
        }
        #endregion

        #region Factory
        public static Box Create(
            Vector2 position,
            Vector2 dimensions,
            CollisionType collisionType,
            Vector2? velocity = null,
            Vector2? acceleration = null,
            IMotionStrategy motionStrategy = null)
        {
            if (motionStrategy == null)
            {
                motionStrategy = new UniformlyAcceleratedMotionStrategy();
            }

            Box box = new Box(motionStrategy)
            {
                Position = position,
                Dimensions = dimensions,
                Velocity = velocity.GetValueOrDefault(Vector2.Zero),
                Acceleration = acceleration.GetValueOrDefault(Vector2.Zero),
            };

            box.CollisionTypes.Add(collisionType);
            return box;
        }

        public static Box Create(float positionX, float positionY, float width, float height, CollisionType collisionType)
        {
            return Box.Create(new Vector2(positionX, positionY), new Vector2(width, height), collisionType);
        }
        #endregion

        #region Utils
        private bool CheckBoxCollision(Box box)
        {
            var edgesVertical = new List<Tuple<Box, float>>()
            {
                Tuple.Create(this, this.LeftBound),
                Tuple.Create(this, this.RightBound),
                Tuple.Create(box, box.LeftBound),
                Tuple.Create(box, box.RightBound)
            };

            var edgesHorizontal = new List<Tuple<Box, float>>()
            {
                Tuple.Create(this, this.LowerBound),
                Tuple.Create(this, this.UpperBound),
                Tuple.Create(box, box.LowerBound),
                Tuple.Create(box, box.UpperBound)
            };

            return Box.SortEdgesAndCheckCollision(edgesVertical)
                   & Box.SortEdgesAndCheckCollision(edgesHorizontal);
        }

        private static bool SortEdgesAndCheckCollision(List<Tuple<Box, float>> edgesVertical)
        {
            edgesVertical.Sort((x, y) => x.Item2.CompareTo(y.Item2));
            return edgesVertical[0].Item1 != edgesVertical[1].Item1;
        }

        private bool CheckGridCollision(IPhysicsObject phobject)
        {
            bool foundCollision = false;
            for (float x = LeftBound; (x <= RightBound && !foundCollision); x += 1.0f)
            {
                for (float y = LowerBound; (y <= UpperBound && !foundCollision); y += 1.0f)
                {
                    foundCollision = phobject.CheckInside(new Vector2(x, y));
                }
            }

            return foundCollision;
        }
        #endregion
    }
}
