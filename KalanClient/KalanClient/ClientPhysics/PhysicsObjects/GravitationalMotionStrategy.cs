﻿using System;
using Microsoft.Xna.Framework;

namespace KalanClient.ClientPsychics.PhysicsObjects
{
    public class GravitationalMotionStrategy : UniformlyAcceleratedMotionStrategy
    {
        private float gravity;

        public GravitationalMotionStrategy(float gravity)
        {
            this.gravity = gravity;
        }

        public override void Update(IPhysicsObject phobject, float secondsSinceLastUpdate)
        {
            if (Math.Abs(phobject.Mass - 0.0f) < float.Epsilon)
            {
                throw new MassZeroException();
            }

            Vector2 accChange = new Vector2(0.0f, -gravity / phobject.Mass);
            phobject.Acceleration -= accChange;
            base.Update(phobject, secondsSinceLastUpdate);
            phobject.Acceleration += accChange;
        }
    }
}
