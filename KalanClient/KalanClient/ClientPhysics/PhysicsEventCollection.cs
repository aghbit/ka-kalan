﻿using System.Collections.Generic;
using KalanClient.ClientPsychics.Events;

namespace KalanClient.ClientPsychics
{
    public class PhysicsEventCollection
    {
        public IList<IPhysicsEvent> PhysicsEvents { get; private set; }

        public PhysicsEventCollection()
        {
            this.PhysicsEvents = new List<IPhysicsEvent>();
        }

        public void Add(IPhysicsEvent phevent) 
        {
            PhysicsEvents.Add(phevent);
        }
    }
}
