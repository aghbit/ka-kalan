﻿using System.Collections.Generic;
using UI;
using PlayerController;
using UI.UIComponents;
using UI.WorldObjects;

namespace KalanClient.API
{
    /// <summary>
    /// Defines an interface for Client API.
    /// </summary>
    public interface IClientApi
    {
        World World { get; }
        Model Model { get; }
        void Connect(string ip, int port);
        void Disconnect();
        void UpdateWorld();
        void PerformActions(IEnumerable<InputAction> actions);

        List<PhysicsObject> NonPlayerObjects { get; }
        List<Player> Players { get; }
    }
}
