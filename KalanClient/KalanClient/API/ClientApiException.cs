﻿using System;

namespace KalanClient.API
{
    public class ClientApiException : Exception
    {
        public ClientApiException(string message) : base(message)
        {
        }

        public ClientApiException(string message, Exception exception) : base(message, exception)
        {            
        }
    }
}
