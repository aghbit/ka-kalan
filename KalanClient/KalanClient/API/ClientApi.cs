﻿using System.Collections.Generic;
using System.Linq;
using Kalan.Net;
using Net.Net;
using Net.Net.Types;
using Protocol;
using UI;
using PlayerController;
using UI.UIComponents;
using UI.WorldObjects;

namespace KalanClient.API
{
    public class ClientApi : IClientApi
    {
        private readonly string _name;

        private readonly World _world;
        public Model Model { get; }

        private NetworkClient _networkClient;
        private ClientDispatcher _clientDispatcher;
        private bool _isConnected;

        public ClientApi(string name)
        {
            _world = new World();
            Model = new Model();
            _name = name;
            _clientDispatcher = new ClientDispatcher(_world, Model, name);
        }

        public World World
        {
            get { return _world; }
        }

        public void Connect(string ip, int port)
        {
            try
            {
                var networkFactory = new NetworkFactory(ip, port);
                _networkClient = (NetworkClient)networkFactory.CreateClient();
                _networkClient.JoinServer();

                var helloPacket = new HelloPacket
                {
                    name = _name.ToCharArray().ToList()
                };
                _networkClient.SendPacket(helloPacket, 0, SendOptions.ReliableInOrder);

                _isConnected = true;
            }
            catch (CouldNotSendMessageException e)
            {
                throw new ClientApiException("Error occured while connecting to the server: network exception thrown while sending a message", e);
            }
        }

        public void Disconnect()
        {
            if (_isConnected)
            {
                _networkClient.LeaveServer();
                _isConnected = false;
            }
            else
            {
                throw new ClientApiException("Error occured while disconnecting from server: client was not connected");
            }
        }

        public void UpdateWorld()
        {
            _networkClient.NetLoop();
            
            NetworkMessage networkMessage;
            while ((networkMessage = _networkClient.Poll()) != null)
            {
                _clientDispatcher.DispatchPacket(networkMessage);
            }
        }

        public void PerformActions(IEnumerable<InputAction> actions)
        {
            var packet = InputActionToPacketMapper.GetUpdatePacket(actions);
            _networkClient.SendPacket(packet, 0, SendOptions.Reliable);
        }

        public List<PhysicsObject> NonPlayerObjects
        {
            get { return _world.NonPlayerObjects; }
        }

        public List<Player> Players
        {
            get { return _world.Players; }
        }
    }
}
