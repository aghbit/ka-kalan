﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ruminate.GUI.Content;
using Ruminate.GUI.Framework;
using UI;
using UI.UIComponents;

namespace KalanClient // TODO: [Tifts] Should be modified and moved to UI, but for now it's impossible
{
    public class MainPage : Page
    {
        private Button NewGameButton { get; set; }
        private Button QuitGameButton { get; set; }
        private Button AuthorsButton { get; set; }
        private Button SettingsButton { get; set; }

        public override void Init(Game game, Texture2D texture, string mapping, SpriteFont font)
        {
            var skin = new Skin(texture, mapping);
            var text = new Text(font, Color.Black);

            PageRoot = new Gui(game, skin, text);
            NewGameButton = new Button(300, 150, 200, "New amazing Kalan Game!", delegate {
                ((ClientGame)game).StartGame(); // [Tifts] This is ass cancer.
            });
            PageRoot.AddWidget(NewGameButton);

            SettingsButton = new Button(300, 200, 200, "Settings", delegate (Widget widget)
            {
                ((Button)widget).Label = "Yet to be here...";
            });
            PageRoot.AddWidget(SettingsButton);

            AuthorsButton = new Button(300, 250, 200, "Credits", delegate (Widget widget)
            {
                ((Button)widget).Label = "Kalan team!";
            });
            PageRoot.AddWidget(AuthorsButton);

            QuitGameButton = new Button(300, 300, 200, "Quit", delegate {
                game.Exit();
            });
            PageRoot.AddWidget(QuitGameButton);
        }

        public override void Update(GameTime time)
        {
            PageRoot.Update();
        }

        public override void Draw()
        {
            PageRoot.Draw();
        }

        public override void Disable()
        {
            foreach (var widget in PageRoot.Widgets)
            {
                widget.Active = false;
            }
        }

        public override void Enable()
        {
            foreach (var widget in PageRoot.Widgets)
            {
                widget.Active = true;
            }
        }
    }
}
