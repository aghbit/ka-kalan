﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Net.Net.Types;
using Physics;
using Protocol;
using Model = UI.UIComponents.Model;
using UI;
using UI.WorldObjects;
using UI.WorldObjects.Pickups;
using UI.WorldObjects.Targets;
using WorldState.Pickups;
using Pickup = UI.WorldObjects.Pickups.Pickup;
using Target = UI.WorldObjects.Targets.Target;
using Wall = UI.WorldObjects.Wall;
using KalanClient.API;
using UI.UIComponents;

namespace KalanClient
{
    public class ClientDispatcher
    {
        private readonly World _world;
        private readonly Model _model;
        public string Name { get; private set; }

        public ClientDispatcher(World world, Model model, string name)
        {
            _world = world;
            _model = model;
            Name = name;
        }

        public bool DispatchPacket(NetworkMessage msg)
        {
            switch (msg.PacketType)
            {
                case PacketType.HelloPacket:
                    return ProcessHelloPacket(msg, msg.Packet as HelloPacket);
                case PacketType.BaseGamestatePacket:
                    return ProcessBaseGamestatePacket(msg, msg.Packet as BaseGamestatePacket);
                case PacketType.UpdateGamestatePacket:
                    return ProcessUpdateGamestatePacket(msg, msg.Packet as UpdateGamestatePacket);
                case PacketType.RejectPacket:
                    return ProcessRejectPacket(msg, msg.Packet as RejectPacket);
                case PacketType.PlayerUpdatePacket:
                    return ProcessPlayerUpdatePacket(msg, msg.Packet as PlayerUpdatePacket);
            }

            return false;
        }

        private bool ProcessHelloPacket(NetworkMessage msg, HelloPacket packet)
        {
            return false;
        }

        private bool ProcessBaseGamestatePacket(NetworkMessage msg, BaseGamestatePacket packet)
        {
            List<Player> players = new List<Player>();

            foreach (var player in packet.gs.players)
            {
                bool local = Name == new String(player.name.ToArray());
                var p = new Player("char", new Vector2(50, 50), 0, local);
                p.Position = new Vector2(packet.gs.phys_objects[player.id].position.x, packet.gs.phys_objects[player.id].position.y);
                p.Velocity = new Vector2(packet.gs.phys_objects[player.id].velocity.x, packet.gs.phys_objects[player.id].velocity.y);
                p.Direction = (packet.gs.players[player.id].flags & PlayerFlags.IsMovingLeft) == PlayerFlags.IsMovingLeft ? SpriteEffects.FlipHorizontally : SpriteEffects.None;
                p.WeaponAngle = packet.gs.players[player.id].shoot_angle;
                p.w = packet.gs.phys_objects[player.id].w;
                p.h = packet.gs.phys_objects[player.id].h;
                p.Eq = packet.gs.players[player.id].eq;
                p.LoadContent();
                players.Add(p);
            }

            if(players.Count > _world.Players.Count) _model.Timer.Start();

            _world.MapSize = new Vector2(packet.map_size.x, packet.map_size.y);
            
            // retrieve walls from server and replace current walls with them
            List<Wall> walls = new List<Wall>();
            List<Bullet> bullets = new List<Bullet>();
            List<Pickup> pickups = new List<Pickup>();
            List<Target> targetsList = new List<Target>();
            Dictionary<short, Target> targets = _world.Targets.ToDictionary(t => t.Id);

            foreach (var physObject in packet.gs.phys_objects)
            {
                Category category = (Category)physObject.category;
                if (category == Category.Walls)
                {
                    Wall wall = new Wall(new Vector2(physObject.position.x, physObject.position.y), physObject.w, physObject.h);
                    wall.LoadContent();
                    walls.Add(wall);
                }
                if (category == Category.Bullets)
                {
                    Bullet bullet = new Bullet(new Vector2(physObject.position.x, physObject.position.y));
                    bullet.LoadContent();
                    bullets.Add(bullet);
                }
                if (category == Category.Targets)
                {
                    if (targets.ContainsKey(physObject.id))
                        targets.Remove(physObject.id);

                    var target = new Target(new Vector2(physObject.position.x, physObject.position.y), physObject.id, physObject.w, physObject.h);
                    target.LoadContent();
                    targetsList.Add(target);
                    
                }
                if (category == Category.Pickup)
                {
                    Pickup pickup;
                    switch ((WorldState.PickupTypes)physObject.subcategory)
                    {
                        case WorldState.PickupTypes.pickupMovementSpeed:
                            pickup = new PickupMovementSpeed(new Vector2(physObject.position.x, physObject.position.y), physObject.id, physObject.w, physObject.h);
                            break;
                        case WorldState.PickupTypes.pickupShotgun:
                            pickup = new PickupShotgun(new Vector2(physObject.position.x, physObject.position.y), physObject.id, physObject.w, physObject.h);
                            break;
                        default:
                            pickup = new PickupShotgun(new Vector2(physObject.position.x, physObject.position.y), physObject.id, physObject.w, physObject.h);
                            break;
                    }
                    
                    pickup.LoadContent();
                    pickups.Add(pickup);
                }
            }
            foreach (var t in targets.Values)
            {
                DestroyedTarget d = new DestroyedTarget(t.Position, t.Width, t.Height);
                d.LoadContent();
                _world.DestroyedTargets.Add(d);
            }

            _world.Players = players;
            _world.Targets = targetsList;
            _world.Walls = walls;
            _world.Bullets = bullets;
            _world.Pickups = pickups;

            var myPlayer = packet.gs.players.FirstOrDefault(player => new String(player.name.ToArray()) == Name);

            _model.Score.score = myPlayer.score;
            
            return false;
        }

        private bool ProcessUpdateGamestatePacket(NetworkMessage msg, UpdateGamestatePacket packet)
        {
            return false;
        }

        private bool ProcessRejectPacket(NetworkMessage msg, RejectPacket packet)
        {
            var sb = new StringBuilder(packet.message.Count);
            packet.message.ForEach(a => sb.Append(a));
            string message = sb.ToString();

            Environment.Exit(0); //TODO: Make some sexy msg box here.
            return true;
        }

        private bool ProcessPlayerUpdatePacket(NetworkMessage msg, PlayerUpdatePacket packet)
        {

            return false;
        }

    }

}

