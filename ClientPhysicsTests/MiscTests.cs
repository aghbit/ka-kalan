﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KalanClient.ClientPsychics;
using KalanClient.ClientPsychics.Events;
using KalanClient.ClientPsychics.PhysicsObjects;

namespace ClientPhysicsTests
{
    [TestClass]
    public class MiscTests
    {
        private static void TestIfStrategyAcceptsNegativeTime(IMotionStrategy strategy)
        {
            IPhysicsObject phobject = new Box(strategy);
            phobject.Mass = 1.0f;
            try
            {
                strategy.Update(phobject, -2.0f);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
            }
        }

        #region Tests
        [TestMethod]
        public void CollisionEventShouldNotAcceptTypeNone()
        {
            CollisionType collisionTypeNone = CollisionType.None;
            IPhysicsObject phobject1 = Box.Create(10, 10, 20, 10, collisionTypeNone);
            IPhysicsObject phobject2 = Box.Create(5, 5, 10, 10, collisionTypeNone);
            try
            {
                CollisionEvent collisionEvent = new CollisionEvent(
                    phobject1,
                    phobject2,
                    collisionTypeNone);
                Assert.Fail();
            }
            catch(ArgumentException)
            {
            }
        }

        [TestMethod]
        public void MotionStrategiesShouldNotAcceptNegativeTime()
        {
            TestIfStrategyAcceptsNegativeTime(new UniformlyAcceleratedMotionStrategy());
            TestIfStrategyAcceptsNegativeTime(new GravitationalMotionStrategy(1.0f));
        }
        #endregion Tests
    }
}
