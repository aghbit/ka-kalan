﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KalanClient.ClientPsychics.PhysicsObjects;
using Microsoft.Xna.Framework;
using KalanClient.ClientPsychics;
using KalanClient.ClientPsychics.Events;

namespace ClientPhysicsTests
{
    [TestClass]
    public class CollisionTests
    {
        #region Utils
        private const float DELTA = 0.1f;

        private static PhysicsEventCollection SimulateTwoObjects(
            IPhysicsObject phobject1, 
            IPhysicsObject phobject2)
        {
            IWorld world = new SimpleWorld();
            world.PhysicsObjects.Add(phobject1);
            world.PhysicsObjects.Add(phobject2);
            PhysicsEventCollection events = Simulator.Simulate(world);
            return events;
        }

        private static PhysicsEventCollection SimulateTwoCollidingBoxes(
            out IPhysicsObject phobject1, 
            out IPhysicsObject phobject2,
            out CollisionType collisionType)
        {
            collisionType = new CollisionType();
            phobject1 = Box.Create(10, 10, 20, 10, collisionType);
            phobject2 = Box.Create(5, 5, 10, 10, collisionType);
            return CollisionTests.SimulateTwoObjects(phobject1, phobject2);
        }
        #endregion

        #region Tests
        [TestMethod]
        public void IntersectingBoxesShouldCollide()
        {
            IPhysicsObject phobject1, phobject2;
            CollisionType collisionType;
            IPhysicsEvent collision = SimulateTwoCollidingBoxes(
                out phobject1, 
                out phobject2,
                out collisionType).PhysicsEvents.First();;
            Assert.IsInstanceOfType(collision, typeof(CollisionEvent));
            Assert.AreEqual(phobject1, collision.InteractingObjects.Item1);
            Assert.AreEqual(phobject2, collision.InteractingObjects.Item2);
        }

        [TestMethod]
        public void DisjointBoxesShouldNotCollide()
        {
            CollisionType collisionType = new CollisionType();
            IPhysicsObject phobject1 = Box.Create(10, 10, 10, 10, collisionType);
            IPhysicsObject phobject2 = Box.Create(30, 30, 10, 10, collisionType);
            PhysicsEventCollection events = CollisionTests.SimulateTwoObjects(phobject1, phobject2);
            Assert.AreEqual(0, events.PhysicsEvents.Count);
        }

        [TestMethod]
        public void ObjectsWithDifferentCollisionTypesShouldNotCollide()
        {
            CollisionType collisionType1 = new CollisionType();
            CollisionType collisionType2 = new CollisionType();
            IPhysicsObject phobject1 = Box.Create(10, 10, 20, 10, collisionType1);
            IPhysicsObject phobject2 = Box.Create(5, 5, 10, 10, collisionType2);
            PhysicsEventCollection events = CollisionTests.SimulateTwoObjects(phobject1, phobject2);
            Assert.AreEqual(0, events.PhysicsEvents.Count);
        }

        [TestMethod]
        public void IntersectingBoxesShouldCollideWhenCornersAreOutside()
        {
            CollisionType collisionType = new CollisionType();
            IPhysicsObject phobject1 = Box.Create(2, 6, 4, 12, collisionType);
            IPhysicsObject phobject2 = Box.Create(2, 6, 12, 4, collisionType);
            PhysicsEventCollection events = CollisionTests.SimulateTwoObjects(phobject1, phobject2);
            IPhysicsEvent collision = events.PhysicsEvents.First();
            Assert.IsInstanceOfType(collision, typeof(CollisionEvent));
            Assert.AreEqual(phobject1, collision.InteractingObjects.Item1);
            Assert.AreEqual(phobject2, collision.InteractingObjects.Item2);
        }

        [TestMethod]
        public void TwoCollidingObjectsShouldGenerateOnlyOneCollisionEvent()
        {
            IPhysicsObject phobject1, phobject2;
            CollisionType collisionType;
            PhysicsEventCollection events = SimulateTwoCollidingBoxes(
                out phobject1, 
                out phobject2,
                out collisionType);
            Assert.AreEqual(1, events.PhysicsEvents.Count);
        }

        [TestMethod]
        public void CollisionDetectionShouldYieldCorrectCollisionType()
        {
            IPhysicsObject phobject1, phobject2;
            CollisionType collisionType;
            PhysicsEventCollection events = SimulateTwoCollidingBoxes(
                out phobject1,
                out phobject2,
                out collisionType);
            CollisionEvent collision = events.PhysicsEvents.First() as CollisionEvent;
            Assert.AreEqual(collisionType, collision.Type);
        }

        [TestMethod]
        public void CollisionShouldStopMovement()
        {
            CollisionType collisionType = new CollisionType();
            IPhysicsObject moving = Box.Create(0.0f, 0.0f, 1.0f, 1.0f, collisionType);
            IPhysicsObject wall = Box.Create(10.0f, 0.0f, 0.2f, 10.0f, collisionType);
            moving.Acceleration = new Vector2(1.5f, 0.0f);
            IWorld world = new SimpleWorld();
            world.PhysicsObjects.Add(moving);
            world.PhysicsObjects.Add(wall);
            world.MillisecondsSinceLastSimulation = 4000;
            Simulator.Simulate(world);
            Assert.AreEqual(9.4f, moving.Position.X, DELTA);
        }

        [TestMethod]
        public void CollisionShouldStopMovementsOfBothObjects()
        {
            CollisionType collisionType = new CollisionType();
            IPhysicsObject moving1 = Box.Create(0.0f, 0.0f, 1.0f, 1.0f, collisionType);
            IPhysicsObject moving2 = Box.Create(10.0f, 0.0f, 1.0f, 1.0f, collisionType);
            moving1.Velocity = new Vector2(1.0f, 0.0f);
            moving2.Velocity = new Vector2(-1.0f, 0.0f);
            IWorld world = new SimpleWorld();
            world.PhysicsObjects.Add(moving1);
            world.PhysicsObjects.Add(moving2);
            world.MillisecondsSinceLastSimulation = 6000;
            Simulator.Simulate(world);
            Assert.AreEqual(4.5f, moving1.Position.X, DELTA);
            Assert.AreEqual(5.5f, moving2.Position.X, DELTA);
        }

        [TestMethod]
        public void CollisionEventShouldHaveCorrectPosition()
        {
            IPhysicsObject phobject1, phobject2;
            CollisionType collisionType;
            IPhysicsEvent collision = SimulateTwoCollidingBoxes(
                out phobject1,
                out phobject2,
                out collisionType).PhysicsEvents.First();
            Assert.AreEqual(5.0f, collision.Position.X, DELTA);
            Assert.AreEqual(7.0f, collision.Position.Y, DELTA);
        }
        #endregion
    }
}
