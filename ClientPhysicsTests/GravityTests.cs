﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace ClientPhysicsTests
{
    [TestClass]
    public class GravityTests
    {
        #region Utils
        private const float DELTA = 0.001f;

        private static IPhysicsObject CreateTestObject()
        {
            IPhysicsObject phobject = Box.Create(
                Vector2.Zero,
                Vector2.Zero,
                CollisionType.None,
                new Vector2(2, 4),
                Vector2.Zero,
                new GravitationalMotionStrategy(1.0f));
            return phobject;
        }

        private static IWorld CreateTestWorld(IPhysicsObject phobject)
        {
            IWorld world = new SimpleWorld();
            world.MillisecondsSinceLastSimulation = 100;
            world.PhysicsObjects.Add(phobject);
            return world;
        }
        #endregion

        #region Tests
        [TestMethod]
        public void GravityShouldChangeVelocity()
        {
            IPhysicsObject phobject = CreateTestObject();
            phobject.Acceleration = Vector2.Zero;
            phobject.Mass = 2.0f;
            IWorld world = CreateTestWorld(phobject);
            Simulator.Simulate(world);
            Assert.AreEqual(new Vector2(2.0f, 3.95f), phobject.Velocity);
            // outside acceleration is not affected by gravity!
            Assert.AreEqual(Vector2.Zero, phobject.Acceleration);
            Assert.AreEqual(2.0f*0.1f, phobject.Position.X, DELTA);
            Assert.AreEqual(0.5f*-0.5f*0.1f*0.1f + 4f*0.1f, phobject.Position.Y, DELTA);
        }

        [TestMethod]
        public void GrivationSimulationWithZeroMassShouldRaiseException()
        {
            IPhysicsObject phobject = CreateTestObject();
            phobject.Mass = 0.0f;
            IWorld world = CreateTestWorld(phobject);
            try
            {
                Simulator.Simulate(world);
                Assert.Fail();
            }
            catch (MassZeroException)
            {
            }
        }
        #endregion
    }
}
