﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KalanClient.ClientPsychics.PhysicsObjects;
using Microsoft.Xna.Framework;
using KalanClient.ClientPsychics;

namespace ClientPhysicsTests
{
    [TestClass]
    public class VelocityTests
    {
        #region Utils
        private const float DELTA = 0.001f;

        private static void SimulateOneObject(IPhysicsObject phobject, int millisSinceLast = 500)
        {
            IWorld world = new SimpleWorld();
            world.MillisecondsSinceLastSimulation = millisSinceLast;
            world.PhysicsObjects.Add(phobject);
            Simulator.Simulate(world);
        }
        #endregion

        #region Tests
        [TestMethod]
        public void VelocityShouldNotChangeWithoutAcceleration()
        {
            IPhysicsObject phobject = new Box(new UniformlyAcceleratedMotionStrategy()) 
            { 
                Velocity = new Vector2(5, 2)
            };

            VelocityTests.SimulateOneObject(phobject);
            Assert.AreEqual(5, phobject.Velocity.X);
            Assert.AreEqual(2, phobject.Velocity.Y);
        }

        [TestMethod]
        public void VelocityChangeShouldBeProportionalToAccelerationAndTime()
        {
            IPhysicsObject phobject = new Box(new UniformlyAcceleratedMotionStrategy())
            {
                Velocity = new Vector2(5, 2),
                Acceleration = new Vector2(1, 1)
            };

            VelocityTests.SimulateOneObject(phobject);
            Assert.AreEqual(5.5, phobject.Velocity.X, DELTA);
            Assert.AreEqual(2.5, phobject.Velocity.Y, DELTA);
        }

        [TestMethod]
        public void PositionChangeShouldBeProportionalToVelocityAndTime()
        {
            IPhysicsObject phobject = new Box(new UniformlyAcceleratedMotionStrategy())
            {
                Position = new Vector2(2, 1),
                Velocity = new Vector2(5, 10),
            };

            VelocityTests.SimulateOneObject(phobject, 200);
            Assert.AreEqual(3, phobject.Position.X, DELTA);
            Assert.AreEqual(3, phobject.Position.Y, DELTA);
        }

        [TestMethod]
        public void PositionChangeShouldBeProportionalToVelocityAfterApplyingAcceleration()
        {
            IPhysicsObject phobject = new Box(new UniformlyAcceleratedMotionStrategy())
            {
                Position = new Vector2(2, 1),
                Velocity = new Vector2(0, 0),
                Acceleration = new Vector2(12, 4)
            };

            VelocityTests.SimulateOneObject(phobject);
            Assert.AreEqual(2.0 + 0.5*12*0.25, phobject.Position.X, DELTA);
            Assert.AreEqual(1.0 + 0.5*4*0.25, phobject.Position.Y, DELTA);
        }
        #endregion
    }
}
