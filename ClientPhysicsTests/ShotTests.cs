﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KalanClient.ClientPsychics.PhysicsObjects;
using KalanClient.ClientPsychics;
using Microsoft.Xna.Framework;
using KalanClient.ClientPsychics.Events;
using System.Linq;

namespace ClientPhysicsTests
{
    [TestClass]
    public class HitTests
    {
        private const float DELTA = 0.01f;

        [TestMethod]
        public void DirectShotShouldHitTarget()
        {
            CollisionType collisionType = new CollisionType();
            IPhysicsObject gun = Box.Create(0.0f, 0.0f, 1.0f, 1.0f, collisionType);
            IPhysicsObject target = Box.Create(10.0f, 0.0f, 1.0f, 1.0f, collisionType);
            IPhysicsObject shot = new Shot(new Vector2(0.0f, 0.0f), 0.0f, collisionType, gun);
            IWorld world = new SimpleWorld();
            world.PhysicsObjects.Add(gun);
            world.PhysicsObjects.Add(target);
            world.PhysicsObjects.Add(shot);
            world.MillisecondsSinceLastSimulation = 40;
            PhysicsEventCollection collection = Simulator.Simulate(world);
            Assert.AreEqual(1, collection.PhysicsEvents.Count);
            IPhysicsEvent phevent = collection.PhysicsEvents.Single();
            Assert.AreSame(target, phevent.InteractingObjects.Item1);
            Assert.AreSame(shot, phevent.InteractingObjects.Item2);
            Assert.AreEqual(10.0f, phevent.Position.X, DELTA);
            Assert.AreEqual(0.0f, phevent.Position.Y, DELTA);
        }

        [TestMethod]
        public void ShotShouldOnlyHitTarget()
        {
            CollisionType collisionType = new CollisionType();
            IPhysicsObject gun = Box.Create(0.0f, 0.0f, 1.0f, 1.0f, collisionType);
            IPhysicsObject target = Box.Create(10.0f, 0.0f, 0.0f, 1.0f, collisionType);
            IPhysicsObject target2 = Box.Create(10.0f, 1.5f, 1.0f, 1.0f, collisionType);
            IPhysicsObject shot = new Shot(new Vector2(0.0f, 0.0f), 0.0f, collisionType, gun);
            IWorld world = new SimpleWorld();
            world.PhysicsObjects.Add(gun);
            world.PhysicsObjects.Add(target);
            world.PhysicsObjects.Add(target2);
            world.PhysicsObjects.Add(shot);
            world.MillisecondsSinceLastSimulation = 40;
            PhysicsEventCollection collection = Simulator.Simulate(world);
            Assert.AreEqual(1, collection.PhysicsEvents.Count);
            IPhysicsEvent phevent = collection.PhysicsEvents.Single();
            Assert.AreSame(target, phevent.InteractingObjects.Item1);
            Assert.AreSame(shot, phevent.InteractingObjects.Item2);
        }
    }
}
