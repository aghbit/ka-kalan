using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Protocol
{
	public enum PacketType
	{
		HelloPacket,
		BaseGamestatePacket,
		UpdateGamestatePacket,
		RejectPacket,
		EndGamePacket,
		PlayerUpdatePacket
	}
	
	public partial class GamePacketHeader
	{
		public PacketType type {get; set;}
		public short length {get; set;}
		public byte seq {get; set;}
	}

	public partial class vec2
	{
		public float x {get; set;}
		public float y {get; set;}
	}

	public partial class HelloPacket
	{
		public List<char> name {get; set;}
	}

	public partial class BaseGamestatePacket
	{
		public List<char> map_name {get; set;}
		public vec2 map_size {get; set;}
		public UpdateGamestatePacket gs {get; set;}
	}

	public partial class UpdateGamestatePacket
	{
		public List<NetPlayer> players {get; set;}
		public List<NetPhysObject> phys_objects {get; set;}
		public List<NetEvent> events {get; set;}
	}

	public partial class RejectPacket
	{
		public List<char> message {get; set;}
	}

	public partial class EndGamePacket
	{
		public int score {get; set;}
	}

	[Flags]
	public enum PlayerUpdateFlags
	{
		Move = 1,
		Jump = 2,
		Shoot = 4,
		RaiseGun = 8,
		LowerGun = 16,
		ChangeWeaponForward = 32,
		ChangeWeaponBackward = 64
	}

	public partial class PlayerUpdatePacket
	{
		public PlayerUpdateFlags flags {get; set;}
		public byte move_dir {get; set;}
	}

	[Flags]
	public enum PlayerFlags
	{
		Delete = 1,
		Dead = 2,
		IsMovingLeft = 4,
		Jump = 8
	}

	public enum Equipment
	{
		Pistol = 1,
		Shotgun = 2
	}

	public partial class NetPlayer
	{
		public PlayerFlags flags {get; set;}
		public Equipment eq {get; set;}
		public byte id {get; set;}
		public byte health {get; set;}
		public int score {get; set;}
		public List<char> name {get; set;}
		public float shoot_angle {get; set;}
	}

	[Flags]
	public enum ObjectFlags
	{
		Delete = 1
	}

	public partial class NetPhysObject
	{
		public ObjectFlags flags {get; set;}
		public short id {get; set;}
		public byte category {get; set;}
		public byte subcategory {get; set;}
		public vec2 position {get; set;}
		public vec2 velocity {get; set;}
		public float w {get; set;}
		public float h {get; set;}
	}

	public enum EventType
	{
		Connect = 1
	}

	public partial class NetEvent
	{
		public EventType type {get; set;}
	}

	public static class WritePDLExtensions
	{
		
		public static byte[] GetPDLBytes(this GamePacketHeader src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.type);
			bw.Write(BitConverter.GetBytes(src.length));
			bw.Write(src.seq);
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this vec2 src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write(BitConverter.GetBytes(src.x));
			bw.Write(BitConverter.GetBytes(src.y));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this HelloPacket src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.name.Count());
			src.name.ForEach(a => bw.Write(a));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this NetPlayer src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.flags);
			bw.Write((byte) src.eq);
			bw.Write(src.id);
			bw.Write(src.health);
			bw.Write(BitConverter.GetBytes(src.score));
			bw.Write((byte) src.name.Count());
			src.name.ForEach(a => bw.Write(a));
			bw.Write(BitConverter.GetBytes(src.shoot_angle));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this NetPhysObject src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.flags);
			bw.Write(BitConverter.GetBytes(src.id));
			bw.Write(src.category);
			bw.Write(src.subcategory);
			bw.Write(GetPDLBytes(src.position));
			bw.Write(GetPDLBytes(src.velocity));
			bw.Write(BitConverter.GetBytes(src.w));
			bw.Write(BitConverter.GetBytes(src.h));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this NetEvent src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.type);
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this UpdateGamestatePacket src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.players.Count());
			src.players.ForEach(a => bw.Write(GetPDLBytes(a)));
			bw.Write(BitConverter.GetBytes((short) src.phys_objects.Count()));
			src.phys_objects.ForEach(a => bw.Write(GetPDLBytes(a)));
			bw.Write((byte) src.events.Count());
			src.events.ForEach(a => bw.Write(GetPDLBytes(a)));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this BaseGamestatePacket src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.map_name.Count());
			src.map_name.ForEach(a => bw.Write(a));
			bw.Write(GetPDLBytes(src.map_size));
			bw.Write(GetPDLBytes(src.gs));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this RejectPacket src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.message.Count());
			src.message.ForEach(a => bw.Write(a));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this EndGamePacket src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write(BitConverter.GetBytes(src.score));
			return ms.ToArray();
		}
		
		public static byte[] GetPDLBytes(this PlayerUpdatePacket src)
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write((byte) src.flags);
			bw.Write(src.move_dir);
			return ms.ToArray();
		}
		
	}
	public partial class GamePacketHeader
	{
		
		public GamePacketHeader()
		{
		}
		public GamePacketHeader(ref ArraySegment<byte> data)
		{
			type = (PacketType) data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			length = BitConverter.ToInt16(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 2, data.Count - 2);
			seq = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
		}
	}
	public partial class vec2
	{
		
		public vec2()
		{
		}
		public vec2(ref ArraySegment<byte> data)
		{
			x = BitConverter.ToSingle(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
			y = BitConverter.ToSingle(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
		}
	}
	public partial class HelloPacket
	{
		
		public HelloPacket()
		{
		}
		public HelloPacket(ref ArraySegment<byte> data)
		{
			var len_name = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			name = new List<char>();
			for (int i = 0; i < len_name; i++)
			{
				name.Add((char) data.Array[data.Offset]);
				data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			}
		}
	}
	public partial class BaseGamestatePacket
	{
		
		public BaseGamestatePacket()
		{
		}
		public BaseGamestatePacket(ref ArraySegment<byte> data)
		{
			var len_map_name = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			map_name = new List<char>();
			for (int i = 0; i < len_map_name; i++)
			{
				map_name.Add((char) data.Array[data.Offset]);
				data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			}
			map_size = new vec2(ref data);
			gs = new UpdateGamestatePacket(ref data);
		}
	}
	public partial class UpdateGamestatePacket
	{
		
		public UpdateGamestatePacket()
		{
		}
		public UpdateGamestatePacket(ref ArraySegment<byte> data)
		{
			var len_players = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			players = new List<NetPlayer>();
			for (int i = 0; i < len_players; i++)
			{
				players.Add(new NetPlayer(ref data));
			}
			var len_phys_objects = BitConverter.ToInt16(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 2, data.Count - 2);
			phys_objects = new List<NetPhysObject>();
			for (int i = 0; i < len_phys_objects; i++)
			{
				phys_objects.Add(new NetPhysObject(ref data));
			}
			var len_events = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			events = new List<NetEvent>();
			for (int i = 0; i < len_events; i++)
			{
				events.Add(new NetEvent(ref data));
			}
		}
	}
	public partial class RejectPacket
	{
		
		public RejectPacket()
		{
		}
		public RejectPacket(ref ArraySegment<byte> data)
		{
			var len_message = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			message = new List<char>();
			for (int i = 0; i < len_message; i++)
			{
				message.Add((char) data.Array[data.Offset]);
				data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			}
		}
	}
	public partial class EndGamePacket
	{
		
		public EndGamePacket()
		{
		}
		public EndGamePacket(ref ArraySegment<byte> data)
		{
			score = BitConverter.ToInt32(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
		}
	}
	public partial class PlayerUpdatePacket
	{
		
		public PlayerUpdatePacket()
		{
		}
		public PlayerUpdatePacket(ref ArraySegment<byte> data)
		{
			flags = (PlayerUpdateFlags) data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			move_dir = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
		}
	}
	public partial class NetPlayer
	{
		
		public NetPlayer()
		{
		}
		public NetPlayer(ref ArraySegment<byte> data)
		{
			flags = (PlayerFlags) data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			eq = (Equipment) data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			id = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			health = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			score = BitConverter.ToInt32(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
			var len_name = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			name = new List<char>();
			for (int i = 0; i < len_name; i++)
			{
				name.Add((char) data.Array[data.Offset]);
				data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			}
			shoot_angle = BitConverter.ToSingle(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
		}
	}
	public partial class NetPhysObject
	{
		
		public NetPhysObject()
		{
		}
		public NetPhysObject(ref ArraySegment<byte> data)
		{
			flags = (ObjectFlags) data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			id = BitConverter.ToInt16(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 2, data.Count - 2);
			category = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			subcategory = data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
			position = new vec2(ref data);
			velocity = new vec2(ref data);
			w = BitConverter.ToSingle(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
			h = BitConverter.ToSingle(data.Array, data.Offset);
			data = new ArraySegment<byte>(data.Array, data.Offset + 4, data.Count - 4);
		}
	}
	public partial class NetEvent
	{
		
		public NetEvent()
		{
		}
		public NetEvent(ref ArraySegment<byte> data)
		{
			type = (EventType) data.Array[data.Offset];
			data = new ArraySegment<byte>(data.Array, data.Offset + 1, data.Count - 1);
		}
	}
}
