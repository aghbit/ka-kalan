﻿using System;

namespace Net.Net.Exceptions
{
    class ServerNotRespondingException : Exception
    {
        public ServerNotRespondingException(string message) : base(message)
        {
        }
    }
}
