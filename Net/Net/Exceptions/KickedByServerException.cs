﻿using System;

namespace Net.Net
{
    class KickedByServerException : Exception
    {
        public KickedByServerException() : base()
        {
        }
    }
}
