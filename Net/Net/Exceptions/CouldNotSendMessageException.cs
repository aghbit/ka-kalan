﻿using System;

namespace Net.Net
{
    public class CouldNotSendMessageException : Exception
    {
        public CouldNotSendMessageException(string message) : base(message)
        {
        }
    }
}
