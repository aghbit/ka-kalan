﻿using System;

namespace Net.Net
{
    class ServerDenyingConnectionException  :Exception
    {
        public ServerDenyingConnectionException() : base()
        {
        }
    }
}
