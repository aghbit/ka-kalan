﻿using System;

namespace Net.Net
{
    class UnhandledMessageTypeException : Exception
    {
        public UnhandledMessageTypeException(string message) : base(message)
        {
        }
    }
}
