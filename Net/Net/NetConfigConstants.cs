﻿namespace Kalan.Net
{
    public class NetConfigConstants
    {
        //For external modules
        public static readonly int PORT = 1992;
        public static readonly string HOST_IP = "127.0.0.1"; //Change this to actual host IP to play multiplayer
        public static readonly string SERVER_IDENTIFIER = "KalanServer";

        //For Net module
        public static readonly byte METADATA_SIZE = 2; // in bytes
        public static readonly byte MAX_SERVER_SILENCE_SECS = 255;
        public static readonly byte MAX_CLIENTS = 255;
        public static readonly byte REMOVE_IDLE_CLIENTS_EVERY_SECS = 5;
        public static readonly byte MAX_CLIENT_IDLE_TIME = 20;


    }
}
