﻿namespace Kalan.Net
{
    public enum ClientMessageType
    {
        JoinGameRequest = 0,
        LeaveGameRequest = 1,
        DataTransfer = 2
    }
}
