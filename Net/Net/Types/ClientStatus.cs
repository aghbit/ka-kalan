﻿namespace Kalan.Net
{
    public enum ClientStatus
    {
        Playing = 0,
        Joined = 1,
        Left = 2
    }
}