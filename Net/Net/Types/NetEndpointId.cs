﻿namespace Net.Net.Types
{
    public class NetEndpointId
    {
        public NetEndpointId(int id)
        {
            _id = id;
        }

        private int _id;
        public override bool Equals(object obj)
        {
            var id = obj as NetEndpointId;
            if (id != null)
                return id._id == _id;

            return false;
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }
}
