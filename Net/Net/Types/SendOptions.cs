﻿using Lidgren.Network;

namespace Kalan.Net
{
    public enum SendOptions
    {
        None = NetDeliveryMethod.ReliableUnordered,
        InOrder = NetDeliveryMethod.ReliableOrdered,
        Reliable = NetDeliveryMethod.ReliableUnordered,
        ReliableInOrder = NetDeliveryMethod.ReliableOrdered
    }
}