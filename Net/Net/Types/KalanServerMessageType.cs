﻿namespace Kalan.Net
{
    public enum KalanServerMessageType
    {
        ConnectionAccepted = 0,
        ConnectionDenied = 1,
        LeaveServer = 2,
        DataTransfer = 3
    }
}
