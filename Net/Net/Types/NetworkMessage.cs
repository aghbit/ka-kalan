﻿using System;
using Kalan.Net;
using Protocol;

namespace Net.Net.Types
{
    public class NetworkMessage
    {

        public Object Packet { get; set; }
        public ClientStatus Status { get; set; }

        public NetEndpointId Id { get; set; }

        public PacketType PacketType { get; set; }

    }
}
