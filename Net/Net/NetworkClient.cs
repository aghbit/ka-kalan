﻿using System;
using System.Collections.Concurrent;
using Lidgren.Network;
using Net.Net;
using Net.Net.Exceptions;
using Net.Net.Types;
using Protocol;

namespace Kalan.Net
{

    public partial class NetworkClient : AbstractClient
    {
        private NetClient _client;
        private readonly string _ip;
        private readonly int _port;
        private readonly ConcurrentQueue<NetworkMessage> _receivedMessages;
        private readonly NetEndpointId _serverConnectionEndpointId  = new NetEndpointId(0);

        NetIncomingMessage _msg;
        DateTime _lastMsgTime = DateTime.Now;


        public NetworkClient(string ip, int port)
        {
            _ip = ip;
            _port = port;
            _receivedMessages = new ConcurrentQueue<NetworkMessage>();
        }


        public NetworkMessage Poll()
        {
            NetworkMessage toReturn;
            if (_receivedMessages.TryDequeue(out toReturn))
            {
                return toReturn;
            }

            return null;
        }

        public void SendData(byte[] data, int channel, SendOptions options)
        {
            var msg = _client.CreateMessage();
            msg.Write((byte) ClientMessageType.DataTransfer);
            msg.Write(data);
            _client.SendMessage(msg, (NetDeliveryMethod)options, channel);
        }

        public void JoinServer()
        {
            var config = new NetPeerConfiguration(NetConfigConstants.SERVER_IDENTIFIER);
            _client = new NetClient(config);
            _client.Start();
            _client.Connect(_ip, _port);

            WaitForHandshakeResponse();
            AskToJoinGame();
            WaitForServerApproval();
        }

        public void LeaveServer()
        {
            var sendMsg = _client.CreateMessage();
            sendMsg.Write((byte) ClientMessageType.LeaveGameRequest);
            var result = _client.SendMessage(sendMsg, NetDeliveryMethod.ReliableOrdered);
            
            Cleanup();
        }

        private void Cleanup()
        {

        }

        private void WaitForHandshakeResponse()
        {
            var acked = false;
            NetIncomingMessage msg;
            //so that server sees us
            DateTime t = DateTime.Now;
            while (!acked)
            {
                CheckTimeoutCondition(t);
                while ((msg = _client.ReadMessage()) != null)
                {
                    if (msg.MessageType == NetIncomingMessageType.StatusChanged)
                    {
                        acked = true;
                    }
                }
            }
        }

        private void AskToJoinGame()
        {
            var sendMsg = _client.CreateMessage();
            sendMsg.Write((byte)ClientMessageType.JoinGameRequest);
            var result = _client.SendMessage(sendMsg, NetDeliveryMethod.ReliableOrdered);

            if (DroppedOrFailed(result))
            {
                throw new CouldNotSendMessageException("Failed to JoinGameRequest. Message status: " + result);
            }
        }

        private byte WaitForServerApproval()
        {
            NetIncomingMessage msg;
            DateTime t = DateTime.Now;
            while (true)
            {
                CheckTimeoutCondition(t);
                while ((msg = _client.ReadMessage()) != null)
                {
                    if (msg.MessageType != NetIncomingMessageType.Data)
                    {
                        continue;
                    }

                    var type = (KalanServerMessageType)msg.ReadByte();
                    switch (type)
                    {
                        case KalanServerMessageType.ConnectionAccepted:
                            return 0;
 
                        case KalanServerMessageType.ConnectionDenied:
                             throw new ServerDenyingConnectionException();

                    }
                    _client.Recycle(msg);
                }
            }
        }

        public void NetLoop()
        {
            CheckTimeoutCondition(_lastMsgTime);
            while ((_msg = _client.ReadMessage()) != null)
            {
                _lastMsgTime = DateTime.Now;
                if (_msg.MessageType != NetIncomingMessageType.Data)
                {
                    continue;
                }

                var type = (KalanServerMessageType) _msg.ReadByte();
                switch (type)
                {
                    case KalanServerMessageType.LeaveServer:
                        throw new KickedByServerException();

                    case KalanServerMessageType.DataTransfer:
                        HandleDataTrasferMessage(_msg);
                        break;
                }
                _client.Recycle(_msg);
            }
          
        }

        private void HandleDataTrasferMessage(NetIncomingMessage msg)
        {
            var networkMessage = BuildNetworkMessage(msg, false);
            //client not present
            if (networkMessage == null)
            {
                return;
            }

            networkMessage.Status = ClientStatus.Playing;
            _receivedMessages.Enqueue(networkMessage);
        }

        private NetworkMessage BuildNetworkMessage(NetIncomingMessage msg, bool allowMissing)
        {
            var gphBytes = msg.ReadBytes(4);
            var gphRef = new ArraySegment<byte>(gphBytes);
            var gph = new GamePacketHeader(ref gphRef);

            var bytesToRead = gph.length;
            var packetBytes = msg.ReadBytes(bytesToRead);

            var networkMessage = new NetworkMessage();
            PacketType pt;
            networkMessage.Id = _serverConnectionEndpointId;
            networkMessage.Packet = DispatchIncomingPacket(gph, packetBytes, out pt);
            networkMessage.PacketType = pt;

            return networkMessage;

        }

        private static bool DroppedOrFailed(NetSendResult result)
        {
            return result == NetSendResult.Dropped || result == NetSendResult.FailedNotConnected;
        }

        private void CheckTimeoutCondition(DateTime t)
        {
            if ((DateTime.Now - t).TotalSeconds > NetConfigConstants.MAX_SERVER_SILENCE_SECS)
            {
                throw new ServerNotRespondingException("Did not hear from server for " + NetConfigConstants.MAX_SERVER_SILENCE_SECS + "seconds");                
            }
        }
    }
}