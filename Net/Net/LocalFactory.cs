﻿namespace Kalan.Net
{
    public class LocalFactory : NetworkFactory
    {
        public LocalFactory(int serverPort) : base(NetConfigConstants.HOST_IP, serverPort)
        {
        }
    }
}