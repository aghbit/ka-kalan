using System;
using System.Collections.Generic;
using Net.Net;
using Lidgren.Network;
using Net.Net.Types;
using Protocol;

namespace Kalan.Net
{
	public partial class NetworkServer : AbstractServer
	{
		public void SendPacket(HelloPacket packet, int channel, SendOptions options, NetEndpointId target)
		{
			var bytes = new List<byte>();
			var gph = new GamePacketHeader() {type = PacketType.HelloPacket};
			var sendBytes = packet.GetPDLBytes();
			gph.length = (short) sendBytes.Length;
			bytes.AddRange(gph.GetPDLBytes());
			bytes.AddRange(sendBytes);
			SendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});
		}
		
		public void SendPacket(BaseGamestatePacket packet, int channel, SendOptions options, NetEndpointId target)
		{
			var bytes = new List<byte>();
			var gph = new GamePacketHeader() {type = PacketType.BaseGamestatePacket};
			var sendBytes = packet.GetPDLBytes();
			gph.length = (short) sendBytes.Length;
			bytes.AddRange(gph.GetPDLBytes());
			bytes.AddRange(sendBytes);
			SendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});
		}
		
		public void SendPacket(UpdateGamestatePacket packet, int channel, SendOptions options, NetEndpointId target)
		{
			var bytes = new List<byte>();
			var gph = new GamePacketHeader() {type = PacketType.UpdateGamestatePacket};
			var sendBytes = packet.GetPDLBytes();
			gph.length = (short) sendBytes.Length;
			bytes.AddRange(gph.GetPDLBytes());
			bytes.AddRange(sendBytes);
			SendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});
		}
		
		public void SendPacket(RejectPacket packet, int channel, SendOptions options, NetEndpointId target)
		{
			var bytes = new List<byte>();
			var gph = new GamePacketHeader() {type = PacketType.RejectPacket};
			var sendBytes = packet.GetPDLBytes();
			gph.length = (short) sendBytes.Length;
			bytes.AddRange(gph.GetPDLBytes());
			bytes.AddRange(sendBytes);
			SendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});
		}
		
		public void SendPacket(EndGamePacket packet, int channel, SendOptions options, NetEndpointId target)
		{
			var bytes = new List<byte>();
			var gph = new GamePacketHeader() {type = PacketType.EndGamePacket};
			var sendBytes = packet.GetPDLBytes();
			gph.length = (short) sendBytes.Length;
			bytes.AddRange(gph.GetPDLBytes());
			bytes.AddRange(sendBytes);
			SendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});
		}
		
		public void SendPacket(PlayerUpdatePacket packet, int channel, SendOptions options, NetEndpointId target)
		{
			var bytes = new List<byte>();
			var gph = new GamePacketHeader() {type = PacketType.PlayerUpdatePacket};
			var sendBytes = packet.GetPDLBytes();
			gph.length = (short) sendBytes.Length;
			bytes.AddRange(gph.GetPDLBytes());
			bytes.AddRange(sendBytes);
			SendData(bytes.ToArray(), channel, options, new List<NetConnection> {ResolveTarget(target)});
		}
		
		public Object DispatchIncomingPacket(GamePacketHeader gph, byte[] packetBytes, out PacketType pt)
		{
			pt = gph.type;
			var ars = new ArraySegment<byte>(packetBytes);
			switch (gph.type)
			{
				case PacketType.HelloPacket:
					return new HelloPacket(ref ars);
				case PacketType.BaseGamestatePacket:
					return new BaseGamestatePacket(ref ars);
				case PacketType.UpdateGamestatePacket:
					return new UpdateGamestatePacket(ref ars);
				case PacketType.RejectPacket:
					return new RejectPacket(ref ars);
				case PacketType.EndGamePacket:
					return new EndGamePacket(ref ars);
				case PacketType.PlayerUpdatePacket:
					return new PlayerUpdatePacket(ref ars);
			}
			
			return null;
		}
		
	}
}
