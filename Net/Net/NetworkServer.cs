﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Lidgren.Network;
using Net.Net;
using Net.Net.Types;
using Protocol;

namespace Kalan.Net
{
    public partial class NetworkServer : AbstractServer
    {
        private NetServer _server;
        private readonly string _ip;
        private readonly int _port;
        private readonly ConcurrentQueue<NetworkMessage> _receivedMessages;
        private readonly Dictionary<NetConnection, NetEndpointId> _clients;
        private readonly Dictionary<NetEndpointId, NetConnection> _clientIds; 
        private readonly Dictionary<NetConnection, DateTime> _lastHeard;
        private NetIncomingMessage _msg;
        private DateTime _idleClientsRemovedAt = DateTime.Now;
        private int _lastClientId;

        private NetConnection ResolveTarget(NetEndpointId id)
        {
            return _clientIds[id];
        }

        public NetworkServer(string ip, int port)
        {
            _ip = ip;
            _lastClientId = 0;
            _port = port;
            _clients = new Dictionary<NetConnection, NetEndpointId>();
            _clientIds = new Dictionary<NetEndpointId, NetConnection>();
            _receivedMessages = new ConcurrentQueue<NetworkMessage>();
            _lastHeard = new Dictionary<NetConnection, DateTime>(NetConfigConstants.MAX_CLIENTS);
        }

        public NetworkMessage Poll()
        {
            NetworkMessage toReturn;
            if (_receivedMessages.TryDequeue(out toReturn))
            {
                return toReturn;
            }

            return null;
        }

        public void SendData(byte[] data, int channel, SendOptions options, IList<NetConnection> receivers)
        {
            var sendMsg = _server.CreateMessage();
            sendMsg.Write((byte) KalanServerMessageType.DataTransfer);
            sendMsg.Write(data);
            _server.SendMessage(sendMsg, receivers, (NetDeliveryMethod) options, channel);
        }

        public void SendData(byte[] data, int channel, SendOptions options)
        {
            SendData(data, channel, options, _clients.Keys.ToList());
        }

        public void StartServer()
        {
            var config = new NetPeerConfiguration(NetConfigConstants.SERVER_IDENTIFIER);
            config.Port = _port;
            config.LocalAddress = IPAddress.Parse(_ip);
            _server = new NetServer(config);
            _server.Start();
        }

        public void StopServer()
        {
            var sendMsg = _server.CreateMessage();
            sendMsg.Write((byte) KalanServerMessageType.LeaveServer);
            _server.SendToAll(sendMsg, NetDeliveryMethod.ReliableUnordered);
        }

        public void Listen()
        {
            if ((DateTime.Now - _idleClientsRemovedAt).TotalSeconds > NetConfigConstants.REMOVE_IDLE_CLIENTS_EVERY_SECS)
            {
                RemoveIdleClients();
                _idleClientsRemovedAt = DateTime.Now;
            }

            while ((_msg = _server.ReadMessage()) != null)
            {
                if (_msg.MessageType != NetIncomingMessageType.Data)
                {
                    continue;
                }

                var type = (ClientMessageType) _msg.ReadByte();
                switch (type)
                {
                    case ClientMessageType.JoinGameRequest:
                        HandleConnectionMessage(_msg);
                        break;

                    case ClientMessageType.LeaveGameRequest:
                        HandleDisconnectMessage(_msg);
                        break;

                    case ClientMessageType.DataTransfer:
                        HandleDataTrasferMessage(_msg);
                        break;

                    default:
                        throw new UnhandledMessageTypeException("Unknown message type: " + type);
                }
                _server.Recycle(_msg);
            }
        }
    

    private NetworkMessage BuildNetworkMessage(NetIncomingMessage msg, bool allowMissing)
    {
        if (!_clients.ContainsKey(msg.SenderConnection) && !allowMissing)
            return null;

        var gphBytes = msg.ReadBytes(4);
        var gphRef = new ArraySegment<byte>(gphBytes);
        var gph = new GamePacketHeader(ref gphRef);

        var bytesToRead = gph.length;
        var packetBytes = msg.ReadBytes(bytesToRead);

        var networkMessage = new NetworkMessage();
        PacketType pt;
        networkMessage.Id = _clients[msg.SenderConnection];
        networkMessage.Packet = DispatchIncomingPacket(gph, packetBytes, out pt);
        networkMessage.PacketType = pt;

        if (networkMessage.Packet == null)
            return null;

        return networkMessage;

        }

        private void HandleConnectionMessage(NetIncomingMessage msg)
        {
            var sender = msg.SenderConnection;
            var sendMsg = _server.CreateMessage();
            if (_clients.Count >= NetConfigConstants.MAX_CLIENTS)
            {
                sendMsg.Write((byte)KalanServerMessageType.ConnectionDenied);
                _server.SendMessage(sendMsg, sender, NetDeliveryMethod.ReliableOrdered);
                return;   
            }

            sendMsg.Write((byte)KalanServerMessageType.ConnectionAccepted);
            var result = _server.SendMessage(sendMsg, sender, NetDeliveryMethod.ReliableOrdered);

            if (DroppedOrFailed(result))
            {
                return;
            }

            _lastHeard[msg.SenderConnection] = DateTime.Now;
            AddClientAndInformUpstream(sender);

        }

        private void HandleDisconnectMessage(NetIncomingMessage msg)
        {
            RemoveClientAndInformUpstream(msg.SenderConnection);
        }

        private void HandleDataTrasferMessage(NetIncomingMessage msg)
        {
            var networkMessage = BuildNetworkMessage(msg, true); //TODO: false
            //client not present
            if (networkMessage == null)
            {
                return;
            }

            networkMessage.Status = ClientStatus.Playing; 
            _receivedMessages.Enqueue(networkMessage);
            _lastHeard[msg.SenderConnection] = DateTime.Now;
        }

        private void RemoveIdleClients()
        {
            DateTime now = DateTime.Now;

            var toRemove = _clients.Where(id => 
                (now - _lastHeard[id.Key]).TotalSeconds > NetConfigConstants.MAX_CLIENT_IDLE_TIME).Select(b => b.Key).ToList();

            toRemove.ForEach(RemoveClientAndInformUpstream);

        }

        private void AddClientAndInformUpstream(NetConnection client)
        {
            var netMessage = new NetworkMessage();
            netMessage.Packet = null;

            int newId = Interlocked.Increment(ref _lastClientId);
            netMessage.Id = new NetEndpointId(newId);
            netMessage.Status = ClientStatus.Joined;

            _clients[client] = netMessage.Id;
            _clientIds[netMessage.Id] = client;
            _receivedMessages.Enqueue(netMessage);
        }

        private void RemoveClientAndInformUpstream(NetConnection client)
        {
            var netMessage = new NetworkMessage();
            netMessage.Packet = null;
            netMessage.Id = _clients[client];
            netMessage.Status = ClientStatus.Left;
            _receivedMessages.Enqueue(netMessage);
            _clientIds.Remove(_clients[client]);
            _clients.Remove(client);
        }

        private static bool DroppedOrFailed(NetSendResult result)
        {
            return result == NetSendResult.Dropped || result == NetSendResult.FailedNotConnected;
        }
        
    }
}