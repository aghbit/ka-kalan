﻿namespace Kalan.Net
{
    public class NetworkFactory : AbstractFactory
    {

        private readonly string serverIP;
        private readonly int serverPort;

        public NetworkFactory(string _hostIP, int _serverPort)
        {
            serverIP = _hostIP;
            serverPort = _serverPort;

        }

        public AbstractClient CreateClient()
        {
            var client = new NetworkClient(serverIP, serverPort);
            return client;
        }

        public AbstractServer CreateServer()
        {
            var server = new NetworkServer(serverIP, serverPort);
            return server;
        }
    }
}