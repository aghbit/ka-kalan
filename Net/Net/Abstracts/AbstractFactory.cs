﻿namespace Kalan.Net
{
    public interface AbstractFactory
    {
        AbstractClient CreateClient();
        AbstractServer CreateServer();
    }
}