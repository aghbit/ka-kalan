﻿using System.Collections.Generic;
using Lidgren.Network;
using Net.Net.Types;

namespace Kalan.Net
{
    public interface AbstractServer
    {
        NetworkMessage Poll();
        void SendData(byte[] data, int channel, SendOptions options,
            IList<NetConnection> receivers);

        void SendData(byte[] data, int channel, SendOptions options);
        void StartServer();
        void StopServer();
    }
}