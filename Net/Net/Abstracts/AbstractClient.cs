﻿using Net.Net.Types;

namespace Kalan.Net
{
    public interface AbstractClient
    {
        NetworkMessage Poll();
        void SendData(byte[] data, int channel, SendOptions options);
        void JoinServer();
        void LeaveServer();
    }
}