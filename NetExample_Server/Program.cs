﻿using System;
using System.Collections.Generic;
using Kalan.Net;
using System.Linq;
using System.Threading;
using Lidgren.Network;
using System.Net;
using Net.Net;
using Net.Net.Types;

namespace NetExample_Server
{
    internal class Program
    {
        public static int counter;
        public static AbstractServer server;
        private static readonly List<NetConnection> _clients = new List<NetConnection>();
        private static int[,] _map = new int[80, 24];

        private static void Main(string[] args)
        {
            var networkFactory = new NetworkFactory("192.168.1.31", NetConfigConstants.PORT);
            server = networkFactory.CreateServer();

            server.StartServer();
            Console.WriteLine("After server start");

            while (true)
            {
                NetworkMessage netMessage = server.Poll();
                if (netMessage == null) continue;

                byte client_id;
                switch (netMessage.Status)
                {
                    case ClientStatus.Joined:
                        Console.WriteLine("Client " + netMessage.Id + " status received");
                        _clients.Add(netMessage.Id);
                        break;

                    case ClientStatus.Left:
                        Console.WriteLine("Client " + netMessage .Id+ " status received");
                        _clients.Remove(netMessage.Id);
                        if (_clients.Count == 0)
                        {
                            Console.WriteLine("Stopping server");
                            Program.server.StopServer();
                            Thread.Sleep(10000);
                            Environment.Exit(0);
                        }
                        break;

                    case ClientStatus.Playing:

                        Console.WriteLine("Got packet: " + ((HelloPacket)(netMessage.Packet)).name + netMessage.PacketType.ToString());
                        break;
                }
            }
        }
    }
}
