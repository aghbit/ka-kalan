﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol;
using WorldState.Weapons;

namespace WorldState
{
    public class Player
    {
        public string Name { get; private set; }
        public int ServerId { get; private set; }
        public NetPlayer NetPlayer { get; private set; }
        public bool FacingRight { get; set; }
        public DateTime LastShootTime { get; set; }
        public bool isSpeedBuffed { get; set; } = false;
        private List<Gun> _availableWeapons = new List<Gun>();
        private byte _currentWeapon;

        public Player(int id, string name)
        {
            Name = name;
            ServerId = id;
            NetPlayer = new NetPlayer {name = new List<char>(name)};
            GunFactory gunFactory = new GunFactory();
            _availableWeapons.Add(gunFactory.CreateGun(Equipment.Pistol)); // magic number cooldown in ms
            NetPlayer.eq = _availableWeapons.ElementAt(0).Type;
            NetPlayer.id = (byte) id;
            NetPlayer.health = 100;
            _currentWeapon = 0;
        }

        public Gun CurrentGun => _availableWeapons[_currentWeapon%_availableWeapons.Count]; //download VS15 plz
     
        public Gun NextGun()
        {
            _currentWeapon++;
            return CurrentGun;
        }

        public Gun PreviousGun()
        {
            _currentWeapon--;
            return CurrentGun;
        }

        public void AddGun(Equipment type)
        {
            GunFactory gunFactory = new GunFactory();
            var gun = gunFactory.CreateGun(type);
            if (_availableWeapons.Find(weapon => weapon.Type == type) == null)
            {
                this._availableWeapons.Add(gun);
            }
        }

        public void ResetGuns()
        {
            _availableWeapons = new List<Gun>();
            GunFactory gunFactory = new GunFactory();
            _availableWeapons.Add(gunFactory.CreateGun(Equipment.Pistol));
            NetPlayer.eq = _availableWeapons.ElementAt(0).Type;
        }

    }
}