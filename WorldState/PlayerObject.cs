﻿using System.Collections.Generic;
using Physics;

namespace WorldState
{
    public class PlayerObject : PhysicsObject
    {
        public PlayerObject(Category cat)
            : base(
                PlayerConstants.PlayerWidth, PlayerConstants.PlayerHeight, Category.Players, PlayerConstants.PlayerMass,
                false, new List<Category>()
                {
                    Category.Players,
                    Category.Targets,
                    Category.Bullets,
                    Category.Walls,
                    Category.Pickup
                })
        {
            
        }
    }
}
