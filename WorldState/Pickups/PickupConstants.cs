﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorldState.Pickups;

namespace WorldState
{
    public class PickupConstants
    {
        public static int initialPickups = 1;
        public int Width { get; set; }
        public int Height { get; set; }

        public static Dictionary<PickupTypes, PickupConstants> PickupDictionary = new Dictionary
            <PickupTypes, PickupConstants>()
        {
            {PickupTypes.pickupMovementSpeed, new PickupConstants(35, 35)},
            {PickupTypes.pickupShotgun, new PickupConstants(35, 35) }
        };

        public static Dictionary<PickupTypes, Type> PickupTypeToClass = new Dictionary<PickupTypes, Type>()
        {
            {PickupTypes.pickupMovementSpeed, typeof (PickupMovementSpeed)},
            {PickupTypes.pickupShotgun, typeof(PickupShotgun) }
        };

    public PickupConstants(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }
    }
}
