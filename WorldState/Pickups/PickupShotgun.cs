﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Protocol;

namespace WorldState.Pickups
{
    class PickupShotgun : Pickup
    {
        public PickupShotgun(Vector2 position, PickupTypes pickupType) : base(position, pickupType)
        {
        }

        public override void PickupFunction(Player player)
        {
            player.AddGun(Equipment.Shotgun);
        }
    }
}
