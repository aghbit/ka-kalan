﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Physics;

namespace WorldState
{
    public abstract class Pickup :PhysicsObject
    {
        public PickupTypes Type { get; set; }
        private static List<Category> _collisions = new List<Category>()
        {
            Category.Players,
            Category.Walls
        };
        protected Pickup(Vector2 position, PickupTypes type)
            : base(PickupConstants.PickupDictionary[type].Width, PickupConstants.PickupDictionary[type].Height, Category.Pickup, 0.001f, false, _collisions)
        {
            this.Type = type;
            this.Position = position;
            this.Velocity = new Vector2(0, 0);
            SubCategory = (byte) type;
            //Logger.Log(LoggingType.Debug, "Add target id:{0}, x:{1}, y:{2}", id, position.X, position.Y);
        }

        public abstract void PickupFunction(Player player);
    }
}
