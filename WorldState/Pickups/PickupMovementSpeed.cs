﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace WorldState.Pickups
{
    class PickupMovementSpeed : Pickup
    {
        public PickupMovementSpeed(Vector2 position, PickupTypes pickupType) : base(position, pickupType) { }

        public override void PickupFunction(Player player)
        {
            player.isSpeedBuffed = true;
        }
    }
}
