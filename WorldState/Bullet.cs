﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Physics;

namespace ServerLogic
{
    public class Bullet : PhysicsObject
    {
        public int PlayerId;

        private static List<Category> _collisions = new List<Category>()
        {
            Category.Bullets,
            Category.Players,
            Category.Targets,
            Category.Walls
        };

        public Bullet(Vector2 position, Vector2 velocity, Vector2 force, int width, int height, float mass, Category cat, int playerId)
            : base(width, height, Category.Bullets, mass, true, _collisions)
        {
            Position = position;
            Force = force;
            Velocity = velocity;
            PlayerId = playerId;
        }

    }
}