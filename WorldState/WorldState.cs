﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using Physics;
using Protocol;

namespace WorldState
{
    public class Worldstate
    {
        public const int MaxPlayers = 64;
        public const int MaxPhysicsObjects = 10240;

        public List<PhysicsObject> PhysObjects { get; set; }
        public List<Player> Players { get; set; }
        private Dictionary<int, PhysicsObject> _physObjectsIds = new Dictionary<int, PhysicsObject>();
        public GameMap Map { get; private set; }

        public IEnumerable<NetPhysObject> NetConv_GetActiveObjects()
        {
            return GetActiveObjects().Select(a => new NetPhysObject()
            {
                position = new vec2() {x = a.Position.X, y = a.Position.Y},
                velocity = new vec2() {x = a.Velocity.X, y = a.Velocity.Y},
                category = (byte) a.Category,
                subcategory = (byte) a.SubCategory,
                w = a.Width,
                h = a.Height,
                id = a.Id
            });
        }

        public PlayerObject GetPlayerObjectForPlayer(Player pl)
        {
            return PhysObjects[pl.ServerId] as PlayerObject;
        }

        public IEnumerable<Player> GetActivePlayers()
        {
            return Players.Where(a => a != null);
        }

        public IEnumerable<PlayerObject> GetActivePlayerObjects()
        {
            return GetActivePlayers().Select(GetPlayerObjectForPlayer);
        }

        public IEnumerable<PhysicsObject> GetActiveObjects()
        {
            return PhysObjects.Where(a => a != null);
        }

        public Worldstate(Texture2D tex)
        {
            Simulation.SetUnitConversionRate();

            PhysObjects = new List<PhysicsObject>();
            Players = new List<Player>();

            PhysObjects.AddRange(Enumerable.Repeat<PhysicsObject>(null, MaxPhysicsObjects));
            Players.AddRange(Enumerable.Repeat<Player>(null, MaxPlayers));

            Map = new GameMap("map1", tex, Category.Walls);
            Map.ReadMap(Category.Walls);

            Map.Walls.ForEach(AddNonplayerPhysObject);
            Map.Targets.ForEach(AddNonplayerPhysObject);
            Map.Pickups.ForEach(AddNonplayerPhysObject);
            //Simulation.Sync();
        }

        public void AddNonplayerPhysObject(PhysicsObject obj)
        {
            int nextId = Enumerable.Range(MaxPlayers, MaxPhysicsObjects).Except(_physObjectsIds.Keys).First();
            _physObjectsIds[nextId] = obj;
            PhysObjects[nextId] = obj;
            obj.Id = (short)nextId;
        }

        public void RemovePlayer(int serverId)
        {
            if (Players[serverId] == null)
                return;

            Players[serverId] = null;
            PhysObjects[serverId] = null;
        }

        public void RemovePhysObject(short objectId)
        {
            if(_physObjectsIds[objectId] == null)
                return;

            _physObjectsIds[objectId] = null;
            PhysObjects[objectId] = null;
        }

        public void AddPlayer(Player newPlayer, PlayerObject playerObject)
        {
            Players[newPlayer.ServerId] = newPlayer;
            PhysObjects[newPlayer.ServerId] = playerObject;
            playerObject.Id = (short)newPlayer.ServerId;
        }
    }
}
