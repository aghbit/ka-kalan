﻿namespace WorldState
{
    public static class PlayerConstants
    {
        public const float PlayerSpeed = 8;
        public const int PlayerHeight = 64;
        public const int PlayerWidth = 32;
        public const float PlayerMass = 11;
        public const float PlayerJumpPower = 250;
    }
}
