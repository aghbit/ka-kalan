﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Physics;

namespace WorldState
{
    public class Target :PhysicsObject
    {
        public int score { get; set; }
        public bool alreadyHit { get; set; } = false;
        private static List<Category> _collisions = new List<Category>()
        {
            Category.Bullets,
            Category.Targets,
            Category.Walls
        };

        public Target(Vector2 position, TargetTypes type)
            : base(TargetConstants.targetsDictionary[type].Width, TargetConstants.targetsDictionary[type].Height, Category.Targets, null, false, _collisions)
        {
            this.score = TargetConstants.targetsDictionary[type].Score;
            this.Position = position;
            this.Velocity = new Vector2(0, 0);
            //Logger.Log(LoggingType.Debug, "Add target id:{0}, x:{1}, y:{2}", id, position.X, position.Y);
        }
    }
}
