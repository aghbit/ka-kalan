﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorldState
{
    public class TargetConstants
    {
        public static int maxTargets = 10;
        public int Width { get; set; }
        public int Height { get; set; }
        public int Score { get; set; }

        public static Dictionary<TargetTypes, TargetConstants> targetsDictionary = new Dictionary <TargetTypes, TargetConstants>()
        {
            {TargetTypes.smallest, new TargetConstants(15, 15, 30)},
            {TargetTypes.medium, new TargetConstants(25, 25, 20)},
            {TargetTypes.biggest, new TargetConstants(35, 35, 10)}
        };

        public TargetConstants(int width, int height, int score)
        {
            this.Width = width;
            this.Height = height;
            this.Score = score;
        }

    }
}
