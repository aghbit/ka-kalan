﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocol;

namespace WorldState.Weapons
{
    class GunFactory
    {
        public Gun CreateGun(Equipment equipment)
        {
            switch (equipment)
            {
                    case Equipment.Pistol:
                        return new Pistol();
                    case Equipment.Shotgun:
                        return new Shotgun();
                    default:
                        return new Pistol();
            }
        }
    }
}
