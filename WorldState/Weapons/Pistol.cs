﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocol;

namespace WorldState.Weapons
{
    class Pistol : Gun
    {
        public Pistol() : base(Equipment.Pistol)
        {
            Cooldown = 300;
        }
    }
}
