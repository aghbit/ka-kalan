﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocol;

namespace WorldState.Weapons
{
    class Shotgun : Gun
    {
        public Shotgun() : base(Equipment.Shotgun)
        {
            Cooldown = 600;
        }
    }
}
