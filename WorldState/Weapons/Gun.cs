﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocol;

namespace WorldState
{
    public abstract class Gun
    {
        public int Cooldown;
        public Equipment Type;  

        protected Gun(Equipment type)
        {
            Type = type;
        }

    }
}
