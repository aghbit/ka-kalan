﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Physics;

namespace WorldState
{
    public class Wall : PhysicsObject
    {
        private static List<Category> _collisions = new List<Category>()
        {
            Category.Bullets,
            Category.Players,
            Category.Targets,
            Category.Walls,
            Category.Pickup
        };

        public Wall(Vector2 position, int width, int height, Category cat)
            : base(width, height, cat, null, false, _collisions)
        {
            Position = position;
        }
    }
}
