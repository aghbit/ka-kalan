﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Physics;

namespace WorldState
{
    public class GameMap
    {
        const uint SpawnPointColor = 0xFF00FF00;
        const uint WallColor = 0xFFFF0000;
        const uint TargetColor = 0xFF0000FF;
        const uint PickupColor = 0xFF00FFFF;
        private readonly Random _random = new Random(DateTime.Now.Millisecond);

        public string Name { get; set; }
        public int SizeY { get; set; }
        public int SizeX { get; set; }
        public Texture2D Bitmap { get; set; }
        public List<Wall> Walls { get; set; }
        public List<Target> Targets { get; set; }
        public List<Pickup> Pickups { get; set; } = new List<Pickup>(); 
        public List<Vector2> CurrentlyAvailableTargetPositions = new List<Vector2>();
        public List<Vector2> CurrentlyAvailablePickupPositions = new List<Vector2>(); 
        private readonly List<Vector2> _spawnPoints = new List<Vector2>(); 

        public GameMap(string name, Texture2D tex, Category wallsCategory)
        {
            Name = name;
            Bitmap = tex;
            Walls = new List<Wall>();
            Targets = new List<Target>();
        }

        public void ReadMap(Category wallsCategory)
        {
            SizeX = Bitmap.Width;
            SizeY = Bitmap.Height;

            var colors = new Color[SizeX*SizeY];

            Bitmap.GetData(colors);

            int width = SizeX;
            int height = SizeY;

            ReadColors(colors, height, width, wallsCategory);
        }

        public void ReadColors(Color[] colors, int height, int width, Category wallsCategory)
        {
            var visited = new bool[width*height];
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var index = x + y*width;

                    if (visited[index]) 
                        continue;

                    switch (colors[index].PackedValue)
                    {
                        case SpawnPointColor:
                            _spawnPoints.Add(new Vector2(x, y));
                            break;
                        case WallColor:
                            ExtractAndAddWall(colors, height, width, wallsCategory, x, y, visited);
                            break;
                        case TargetColor:
                            //Logger.Log(LoggingType.Debug, "New target x:{0}, y:{1}", x, y);
                            CurrentlyAvailableTargetPositions.Add(new Vector2(x, y));
                            break;
                        case PickupColor:
                            CurrentlyAvailablePickupPositions.Add(new Vector2(x, y));
                            break;


                    }

                    visited[index] = true;
                }
            }
            CreateInitialTargets(TargetConstants.maxTargets);
            this.Pickups = CreatePickupObjects(PickupConstants.initialPickups);
        }

        private void CreateInitialTargets(int maxTargets)
        {
            for (int i = 0; i < maxTargets; i++)
            {
                Vector2 pos = this.GetSensinbleTargetPosition();
                Array values = Enum.GetValues(typeof(TargetTypes));
                Targets.Add(new Target(pos, (TargetTypes)values.GetValue(_random.Next(0, values.Length))));
                //Targets.Add(new Target(pos));
                this.CurrentlyAvailableTargetPositions.Remove(pos);
            }
        }

        public List<Pickup> CreatePickupObjects (int amount)
        {
            var result = new List<Pickup>();
            Random random = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < amount; i++)
            {
                Vector2 pos = this.GetSensiblePickupPosition();
                Array values = Enum.GetValues(typeof (PickupTypes));
                PickupTypes type = (PickupTypes) values.GetValue(_random.Next(0, values.Length));
                Pickup pickupObject = (Pickup) Activator.CreateInstance(PickupConstants.PickupTypeToClass[type], pos, type);
                result.Add(pickupObject);
                this.CurrentlyAvailablePickupPositions.Remove(pos);
            }
            return result;
        }

        private void ExtractAndAddWall(Color[] colors, int height, int width, Category wallsCategory, int x, int y,
            bool[] visited)
        {
            Func<int, int, int> getFurthestWallPixelAfter = (row, xs) =>
                Enumerable.Range(xs, width - xs).TakeWhile(a => colors[a + row * width].PackedValue == WallColor).Last();
            Func<int, int, int> getLowestWallPixelAfter = (col, ys) =>
                Enumerable.Range(ys, height - ys).TakeWhile(a => colors[col + a * width].PackedValue == WallColor).Last();

            int yExtreme = getLowestWallPixelAfter(x, y);
            int xExtreme = Enumerable.Range(y, yExtreme - y + 1).Min(a => getFurthestWallPixelAfter(a, x));

            for (int j = y; j <= yExtreme; j++)
                for (int i = x; i <= xExtreme; i++)
                    visited[i + j * width] = true;

            width = xExtreme - x + 1;
            height = yExtreme - y + 1;
            Walls.Add(new Wall(new Vector2(x + width / 2, y + height / 2), width, height, wallsCategory));
        }

        public Vector2 GetSensibleSpawnPosition()
        {
            return _spawnPoints[new Random().Next(_spawnPoints.Count)];
        }

        public Vector2 GetSensinbleTargetPosition()
        {
            return CurrentlyAvailableTargetPositions[_random.Next(CurrentlyAvailableTargetPositions.Count)];
        }

        public Vector2 GetSensiblePickupPosition()
        {
            return CurrentlyAvailablePickupPositions[_random.Next(CurrentlyAvailablePickupPositions.Count)];
        }
    }
}