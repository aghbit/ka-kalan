﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Physics;

namespace ServerPhysicsTest
{
    [TestClass]
    public class CategoryTest
    {
        [TestMethod]
        public void TestShouldMatchLoneMatchingObject()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);
            
            bool matched = Category.Players.Equals(obj.Category);

            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void TestShouldntMatchObjectIfNotMember()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);

            bool matched = Category.Walls.Equals(obj.Category);

            Assert.IsFalse(matched);
        }
    }
}
