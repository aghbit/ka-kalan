﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Physics;

namespace ServerPhysicsTest
{
    [TestClass]
    public class SimulationTest
    {
        [TestMethod]
        public void TestShouldAddBodyOnFirstFrame()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);
            HashSet<PhysicsObject> set = new HashSet<PhysicsObject>();
            set.Add(obj);
            Simulation sim = new Simulation(set);

            sim.Simulate(0.01f);

            Assert.IsTrue(sim.Contains(obj));
        }

        [TestMethod]
        public void TestShouldPersistBody()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);
            HashSet<PhysicsObject> set = new HashSet<PhysicsObject>();
            set.Add(obj);
            Simulation sim = new Simulation(set);

            sim.Simulate(0.01f);
            sim.Simulate(0.01f);

            Assert.IsTrue(sim.Contains(obj));
        }

        [TestMethod]
        public void TestShouldAddBodyOnFirstFrameAfterCreation()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);
            HashSet<PhysicsObject> set = new HashSet<PhysicsObject>();
            Simulation sim = new Simulation(set);

            sim.Simulate(0.01f);
            set.Add(obj);
            sim.Simulate(0.01f);

            Assert.IsTrue(sim.Contains(obj));
        }

        [TestMethod]
        public void TestShouldDeleteBodyOnFirstFrameAfterDestruction()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);
            HashSet<PhysicsObject> set = new HashSet<PhysicsObject>();
            set.Add(obj);
            Simulation sim = new Simulation(set);

            sim.Simulate(0.01f);
            set.Remove(obj);
            sim.Simulate(0.01f);

            Assert.IsFalse(sim.Contains(obj));
        }

        [TestMethod]
        public void TestShouldDetectOverlappingAsCollision()
        {
            List<PhysicsObject> set = new List<PhysicsObject>();
            PhysicsObject obj1 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj1);
            PhysicsObject obj2 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj2);
            Simulation sim = new Simulation(set);
            sim.Sync();

            obj1.Position = new Vector2(0, 0);
            obj2.Position = new Vector2(5, 5);
            sim.Simulate(0.01f);

            List<CollisionEvent> clist = sim.GetCollisions().GetCollisionsOf(obj1, obj2);
            Assert.IsTrue(clist.Count == 1 && (
                ((clist[0].Object1 == obj1) && (clist[0].Object2 == obj2)) ||
                ((clist[0].Object1 == obj2) && (clist[0].Object2 == obj1))
            ));
        }

        [TestMethod]
        public void TestShouldntDetectDisjointAsCollision()
        {
            List<PhysicsObject> set = new List<PhysicsObject>();
            PhysicsObject obj1 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj1);
            PhysicsObject obj2 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj2);
            Simulation sim = new Simulation(set);
            sim.Sync();

            obj1.Position = new Vector2(0, 0);
            obj2.Position = new Vector2(10.03f, 0);
            sim.Simulate(0.01f);

            List<CollisionEvent> clist = sim.GetCollisions().GetCollisionsOf(obj1, obj2);
            Assert.AreEqual(0, clist.Count);
        }

        [TestMethod]
        public void TestShouldntDetectVeryFarObjectsAsCollision()
        {
            List<PhysicsObject> set = new List<PhysicsObject>();
            PhysicsObject obj1 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj1);
            PhysicsObject obj2 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj2);
            Simulation sim = new Simulation(set);
            sim.Sync();

            obj1.Position = new Vector2(0, 0);
            obj2.Position = new Vector2(100, 100);
            sim.Simulate(0.01f);

            List<CollisionEvent> clist = sim.GetCollisions().GetCollisionsOf(obj1, obj2);
            Assert.AreEqual(0, clist.Count);
        }

        [TestMethod]
        public void TestShouldDetectTouchingAsCollision()
        {
            List<PhysicsObject> set = new List<PhysicsObject>();
            PhysicsObject obj1 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj1);
            PhysicsObject obj2 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj2);
            Simulation sim = new Simulation(set);
            sim.Sync();

            obj1.Position = new Vector2(0, 0);
            obj2.Position = new Vector2(10, 0);
            sim.Simulate(0.01f);

            List<CollisionEvent> clist = sim.GetCollisions().GetCollisionsOf(obj1, obj2);
            Assert.IsTrue(clist.Count == 1 && (
                ((clist[0].Object1 == obj1) && (clist[0].Object2 == obj2)) ||
                ((clist[0].Object1 == obj2) && (clist[0].Object2 == obj1))
            ));
        }

        [TestMethod]
        public void TestShouldDetectCornerTouchingAsCollision()
        {
            List<PhysicsObject> set = new List<PhysicsObject>();
            PhysicsObject obj1 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj1);
            PhysicsObject obj2 = new PhysicsObject(10, 10, Category.Players, 1, false);
            set.Add(obj2);
            Simulation sim = new Simulation(set);
            sim.Sync();

            obj1.Position = new Vector2(0, 0);
            obj2.Position = new Vector2(10, 10);
            sim.Simulate(0.01f);

            List<CollisionEvent> clist = sim.GetCollisions().GetCollisionsOf(obj1, obj2);
            Assert.IsTrue(clist.Count == 1 && (
                ((clist[0].Object1 == obj1) && (clist[0].Object2 == obj2)) ||
                ((clist[0].Object1 == obj2) && (clist[0].Object2 == obj1))
            ));
        }

        [TestMethod]
        public void TestShouldDetectHit()
        {
            PhysicsObject source = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject target = new PhysicsObject(1, 10, Category.Players, 1, false);
            List<PhysicsObject> set = new List<PhysicsObject>();
            set.Add(source);
            set.Add(target);
            PhysicsObject bullet = new PhysicsObject(1, 1, Category.Players, 1, true);
            set.Add(bullet);
            Simulation sim = new Simulation(set);
            sim.Sync();

            source.Position = new Vector2(0, 0);
            target.Position = new Vector2(5, 0);
            bullet.Position = new Vector2(source.Position.X + 1, source.Position.Y);
            bullet.Velocity = new Vector2(100, 0);
            for (int i = 0; i < 100; i++)
            {
                sim.Simulate(0.01f);
            }            
            List<CollisionEvent> clist = sim.GetCollisions().GetCollisionsOf(bullet, target);
            Assert.IsTrue(clist.Count == 1 && (
                ((clist[0].Object1 == bullet) && (clist[0].Object2 == target)) ||
                ((clist[0].Object1 == target) && (clist[0].Object2 == bullet))
            ));
        }

    }
}
