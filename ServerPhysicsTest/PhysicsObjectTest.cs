﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Physics;

namespace ServerPhysicsTest
{
    [TestClass]
    public class PhysicsObjectTest
    {
        [TestMethod]
        public void TestShouldMatchSelf()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);

            bool matched = obj.Matches(obj);

            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void TestShouldntMatchAnother()
        {
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Players, 1, false);

            bool matched = obj2.Matches(obj1);

            Assert.IsFalse(matched);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidBindingException))]
        public void TestShouldFailWhenBindingManually()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);
            Simulation sim = new Simulation(new List<PhysicsObject>());

            obj.Bind(sim);

            // InvalidBindingException expected
            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(AlreadyBoundException))]
        public void TestShouldFailWhenBindingToAnotherSimulation()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players,  1, false);
            List<PhysicsObject> set1 = new List<PhysicsObject>();
            Simulation sim1 = new Simulation(set1);
            List<PhysicsObject> set2 = new List<PhysicsObject>();
            Simulation sim2 = new Simulation(set2);

            set1.Add(obj);
            set2.Add(obj);
            sim1.Simulate(0.01f);
            sim2.Simulate(0.01f);

            // AlreadyBoundException expected
            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(NotBoundException))]
        public void TestShouldFailWhenPresimulatedButNotBound()
        {
            PhysicsObject obj = new PhysicsObject(1, 1, Category.Players, 1, false);

            obj.PreSimulation();

            // NotBoundException expected
            Assert.Fail();
        }
    }
}
