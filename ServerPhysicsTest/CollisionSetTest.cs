﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Physics;

namespace ServerPhysicsTest
{
    [TestClass]
    public class CollisionSetTest
    {
        [TestMethod]
        public void TestSubsetMatchingWithConcreteObjectsExpectingMatchWithOnly1Possible()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Players, 1, false);
            CollisionEvent actualEvent = new CollisionEvent(obj1, obj2);
            set.Add(actualEvent);

            List<CollisionEvent> events = set.GetCollisionsOf(obj1, obj2);

            Assert.AreEqual(events.Count, 1);
            Assert.AreEqual(events[0], actualEvent);
        }

        [TestMethod]
        public void TestSubsetMatchingWithTwoConcreteObjectsExpectingNoMatches()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj3 = new PhysicsObject(1, 1, Category.Players, 1, false);
            CollisionEvent actualEvent = new CollisionEvent(obj1, obj2);
            set.Add(actualEvent);

            List<CollisionEvent> events = set.GetCollisionsOf(obj1, obj3);

            Assert.AreEqual(events.Count, 0);
        }

        [TestMethod]
        public void TestSubsetMatchingWithTwoConcreteObjectsExpectingMatchWithMultiplePossibilities()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj3 = new PhysicsObject(1, 1, Category.Players, 1, false);
            CollisionEvent actualEvent1 = new CollisionEvent(obj1, obj2);
            CollisionEvent actualEvent2 = new CollisionEvent(obj1, obj3);
            CollisionEvent actualEvent3 = new CollisionEvent(obj3, obj2);
            set.Add(actualEvent1);
            set.Add(actualEvent2);
            set.Add(actualEvent3);

            List<CollisionEvent> events = set.GetCollisionsOf(obj1, obj3);

            Assert.AreEqual(events.Count, 1);
            Assert.AreEqual(events[0], actualEvent2);
        }

        [TestMethod]
        public void TestSubsetMatchingWithCategoryAndConcreteObjectExpectingMatchWithOnlyOnePossible()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Targets, 1, false);
            CollisionEvent actualEvent = new CollisionEvent(obj1, obj2);
            set.Add(actualEvent);

            List<CollisionEvent> events = set.GetCollisionsOf(Category.Players, obj2);

            Assert.AreEqual(events.Count, 1);
            Assert.AreEqual(events[0], actualEvent);
        }
        
        [TestMethod]
        public void TestSubsetMatchingWithCategoryAndConcreteObjectExpectingNoMatches()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Walls, 1, false);
            CollisionEvent actualEvent = new CollisionEvent(obj1, obj2);
            set.Add(actualEvent);

            List<CollisionEvent> events = set.GetCollisionsOf(Category.Targets, obj2);

            Assert.AreEqual(events.Count, 0);
        }

        [TestMethod]
        public void TestSubsetMatchingWithCategoryAndConcreteObjectExpectingMatchWithMultiplePossibilities()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Targets, 1, false);
            PhysicsObject obj3 = new PhysicsObject(1, 1, Category.Players, 1, false);
            CollisionEvent actualEvent1 = new CollisionEvent(obj1, obj2);
            CollisionEvent actualEvent2 = new CollisionEvent(obj1, obj3);
            CollisionEvent actualEvent3 = new CollisionEvent(obj2, obj3);
            set.Add(actualEvent1);
            set.Add(actualEvent2);
            set.Add(actualEvent3);

            List<CollisionEvent> events = set.GetCollisionsOf(Category.Players, obj3);

            Assert.AreEqual(events.Count, 1);
            Assert.AreEqual(events[0], actualEvent2);
        }
        [TestMethod]
        public void TestSubsetMatchingWithCategoryAndConcreteObjectExpectingMultipleMatches()
        {
            CollisionSet set = new CollisionSet();
            PhysicsObject obj1 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj2 = new PhysicsObject(1, 1, Category.Players, 1, false);
            PhysicsObject obj3 = new PhysicsObject(1, 1, Category.Walls, 1, false);
            CollisionEvent actualEvent1 = new CollisionEvent(obj1, obj2);
            CollisionEvent actualEvent2 = new CollisionEvent(obj1, obj3);
            CollisionEvent actualEvent3 = new CollisionEvent(obj2, obj3);
            set.Add(actualEvent1);
            set.Add(actualEvent2);
            set.Add(actualEvent3);

            List<CollisionEvent> events = set.GetCollisionsOf(Category.Players, obj3);

            Assert.AreEqual(events.Count, 2);
            Assert.IsTrue(events.Contains(actualEvent2));
            Assert.IsTrue(events.Contains(actualEvent3));
        }
    }
}
