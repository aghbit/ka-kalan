﻿using Kalan.Net;
using KalanServer.ServerLogic;
using KalanServer.ServerLogic.Commands;
using Microsoft.Xna.Framework;
using Moq;
using NUnit.Framework;

namespace ServerLogicTest
{
    [TestFixture]
    public class SerializationTest
    {
        private readonly Mock<NetworkClientEndpoint> _clientEndpointMock;

        public SerializationTest()
        {
            _clientEndpointMock = new Mock<NetworkClientEndpoint>();
        }

        [Test]
        public void ShouldSerializeThenDeserialize()
        {
            // given
            var positionCommand = new PositionCommand {MoveDirection = new Vector2(1f, 0f), ViewAngle = 0};

            // when
            byte[] bytes = NetworkCommandSerializer.Serialize(positionCommand);
            PositionCommand result = NetworkCommandDeserializer.Deserialize(bytes,
                new EndpointID(_clientEndpointMock.Object));

            // then
            Assert.AreEqual(result, positionCommand);
        }
    }
}