﻿using System.Runtime.InteropServices;
using System.Windows.Forms;
using KalanServer;
using KalanServer.ServerLogic;
using KalanServer.ServerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using Color = Microsoft.Xna.Framework.Color;

namespace ServerLogicTest
{
    [TestFixture]
    public class GameMapTest
    {
        private readonly GameMap _gameMap = new GameMap(null, new Category("walls"));
        [Test]
        public void ShouldReadSpawnPoints()
        {
            // given
            var colors = CreateMap();

            // when
            _gameMap.ReadColors(colors, 512, 512, new Category("walls"));

            // then
            Assert.IsNotNull(_gameMap.GetSensibleSpawnPosition());
        }

        private static Color[] CreateMap()
        {
            var colors = new Color[512*512];
            colors[10].G = 255;
            CreateWall(colors, 512, new Vector2(100, 100), new Vector2(200, 200));
            return colors;
        }

        private static void CreateWall(Color[] colors, int mapWidth, Vector2 begin, Vector2 end)
        {
            for (var i = (int) begin.X; i < begin.Y; i++)
            {
                for (var j = (int) end.X; j < end.Y; j++)
                {
                    colors[i + j*mapWidth].B = 255;
                }
            }
        }
    }
}