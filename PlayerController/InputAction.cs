﻿namespace PlayerController
{
    public enum InputAction { None, MoveLeft, MoveRight, Jump, Shoot, RaiseGun, LowerGun, ChangeWeaponForward, ChangeWeaponBackward }
}
