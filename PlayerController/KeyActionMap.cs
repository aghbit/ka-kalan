﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace PlayerController
{
    public class KeyActionMap
    {
        public Dictionary<Keys, InputAction> Map { get; private set; }

        public KeyActionMap()
        {
            Map = new Dictionary<Keys, InputAction>
            {
                {Keys.Left, InputAction.MoveLeft},
                {Keys.Right, InputAction.MoveRight},
                {Keys.Space, InputAction.Jump},
                {Keys.A, InputAction.Shoot },
                {Keys.Up, InputAction.RaiseGun },
                {Keys.Down, InputAction.LowerGun },
                {Keys.Q, InputAction.ChangeWeaponBackward },
                {Keys.E, InputAction.ChangeWeaponForward }
            };
        }

        public InputAction this[Keys key]
        {
            get
            {
                return Map.ContainsKey(key) ? Map[key] : InputAction.None;
            }
        }
    }
}
