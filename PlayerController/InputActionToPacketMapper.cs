﻿using System.Collections.Generic;
using Protocol;

namespace PlayerController
{
    public class InputActionToPacketMapper
    {
        public static PlayerUpdatePacket GetUpdatePacket(IEnumerable<InputAction> actions)
        {
            var packet = new PlayerUpdatePacket();
            foreach (var action in actions)
            {
                switch (action)
                {
                    case InputAction.MoveLeft:
                        packet.flags |= PlayerUpdateFlags.Move;
                        packet.move_dir = 0;
                        break;
                    case InputAction.MoveRight:
                        packet.flags |= PlayerUpdateFlags.Move;
                        packet.move_dir = 1;
                        break;
                    case InputAction.Jump:
                        packet.flags |= PlayerUpdateFlags.Jump;
                        break;
                    case InputAction.Shoot:
                        packet.flags |= PlayerUpdateFlags.Shoot;
                        break;
                    case InputAction.LowerGun:
                        packet.flags |= PlayerUpdateFlags.LowerGun;
                        break;
                    case InputAction.RaiseGun:
                        packet.flags |= PlayerUpdateFlags.RaiseGun;
                        break;
                    case InputAction.ChangeWeaponForward:
                        packet.flags |= PlayerUpdateFlags.ChangeWeaponForward;
                        break;
                    case InputAction.ChangeWeaponBackward:
                        packet.flags |= PlayerUpdateFlags.ChangeWeaponBackward;
                        break;
                }
            }
            return packet;
        }
    }
}
