﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Input;

namespace PlayerController
{
    public class InputHandler
    {
        private KeyActionMap _map;

        private Dictionary<InputAction, bool> _actionsWithCooldown = new Dictionary<InputAction, bool>();

        public InputHandler(KeyActionMap map)
        {
            _map = map;
            _actionsWithCooldown.Add(InputAction.ChangeWeaponBackward, false);
            _actionsWithCooldown.Add(InputAction.ChangeWeaponForward, false);
        }

        public IEnumerable<InputAction> HandleInput()
        {
            KeyboardState keyboardState = Keyboard.GetState();

            Keys[] pressedKeys = keyboardState.GetPressedKeys();

            var actions = pressedKeys.Select(k => _map[k]).ToList();

            var listCooldownActions = new List<InputAction>(_actionsWithCooldown.Keys);
            foreach (var action in listCooldownActions)
            {
                if (!actions.Contains(action))
                {
                    _actionsWithCooldown[action] = false;
                }
                else
                {
                    if (_actionsWithCooldown[action]) actions.Remove(action);
                    else _actionsWithCooldown[action] = true;
                }
            }


            return actions;
        }
    }
}
