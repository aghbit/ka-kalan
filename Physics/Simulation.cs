﻿using System.Collections.Generic;
using System.Linq;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;

namespace Physics
{
    public class Simulation : HashSet<PhysicsObject>
    {
        public World World { get; private set; }
        protected IEnumerable<PhysicsObject> Watched;

        public Simulation(IEnumerable<PhysicsObject> watched)
        {
            this.World = new World(new Vector2(0, 9.8f)); //value of gravity is magic number, set without thinking
            this.Watched = watched;
        }

        public static void SetUnitConversionRate()
        {
            
            ConvertUnits.SetDisplayUnitToSimUnitRatio(20f); //this value is also magic
        }

        protected PhysicsObject FindByBody(Body body)
        {
            return this.FirstOrDefault(obj => obj.Body == body);
        }

        public void Sync()
        {

            foreach (PhysicsObject watchee in this.Watched.Where(a => a != null))
            {
                if (!this.Contains(watchee))
                {
                    this.Add(watchee);
                }
            }

            List<PhysicsObject> toBeRemoved = this.Where(obj => !this.Watched.Contains(obj)).ToList();
            foreach (PhysicsObject obj in toBeRemoved)
            {
                this.Remove(obj);
            }
        }

        public void Simulate(float seconds)
        {
            this.Sync();
            foreach (PhysicsObject obj in this)
            {
                obj.PreSimulation();
            }
            this.World.Step(seconds);
        }

        public CollisionSet GetCollisions()
        {
            CollisionSet set = new CollisionSet();
            foreach (Contact contact in this.World.ContactList)
            {
                if (!contact.IsTouching)
                {
                    continue;
                }
                List<IEventParticipant> participants = new List<IEventParticipant>();
                PhysicsObject part1 = this.FindByBody(contact.FixtureA.Body);
                PhysicsObject part2 = this.FindByBody(contact.FixtureB.Body);
                CollisionEvent collision = new CollisionEvent(part1, part2);
                set.Add(collision);
            }
            return set;
        }

        public new bool Add(PhysicsObject obj)
        {
            bool success = base.Add(obj);
            if (success)
            {
                obj.Bind(this);
            }
            return success;
        }

        public new bool Remove(PhysicsObject obj)
        {
            bool success = base.Remove(obj);
            if (success)
            {
                this.World.RemoveBody(obj.Body);
            }
            return success;
        }
    }
}
