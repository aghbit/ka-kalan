﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;

namespace Physics
{
    public class PhysicsObject : IEventParticipant
    {
        protected short id;
        public short Id
        {
            get { return id; }
            set { if (id == -1) id = value; }
        }


        protected float width;
        public float Width
        {
            get { return ConvertUnits.ToDisplayUnits(this.width); }
            set { this.width = ConvertUnits.ToSimUnits(value); }
        }

        protected float height;
        public float Height
        {
            get { return ConvertUnits.ToDisplayUnits(this.height); }
            set { this.height = ConvertUnits.ToSimUnits(value); }
        }
        public float? Mass { get; set; }
        public bool stand { get; set; }

        protected Vector2 preBindPosition = Vector2.Zero;
        protected Vector2 preBindVelocity = Vector2.Zero;

        public Vector2 Position {
            get {
                if (Equals(this.Body, default(Body)))
                    return ConvertUnits.ToDisplayUnits(this.preBindPosition);
                else
                    return ConvertUnits.ToDisplayUnits(this.Body.Position);
            }
            set {
                if (Equals(this.Body, default(Body)))
                    this.preBindPosition = ConvertUnits.ToSimUnits(value);
                else
                    this.Body.Position = ConvertUnits.ToSimUnits(value);
            }
        }
        public Vector2 Velocity {
            get {
                if (Equals(this.Body, default(Body)))
                    return ConvertUnits.ToDisplayUnits(this.preBindVelocity);
                else
                    return ConvertUnits.ToDisplayUnits(this.Body.LinearVelocity);
            }
            set {
                if (Equals(this.Body, default(Body)))
                    this.preBindVelocity = ConvertUnits.ToSimUnits(value);
                else
                    this.Body.LinearVelocity = ConvertUnits.ToSimUnits(value);
            }
        }
        protected Vector2 force;
        public Vector2 Force {
            get { return ConvertUnits.ToDisplayUnits(this.force); }
            set { this.force = ConvertUnits.ToSimUnits(value); }
        }

        protected bool fastMoving;

        public Category Category { get; private set; }
        public byte SubCategory { get; set; }

        public Body Body { get; set; }

        public FarseerPhysics.Dynamics.Category Collisions { get; set; } 

        public PhysicsObject(float width, float height, Category category, float? mass, bool fastMoving, List<Category> collisions)
        {
            this.Collisions = FarseerPhysics.Dynamics.Category.None;

            this.Width = width;
            this.Height = height;
            this.Mass = mass;
            this.fastMoving = fastMoving;
            this.Category = category;
            this.Force = Vector2.Zero;
            this.id = -1;
            foreach (Category col in collisions)
            {
                this.Collisions |= (FarseerPhysics.Dynamics.Category)col;
            }
        }

        public PhysicsObject(float width, float height, Category category, float? mass, bool fastMoving)
        {
            this.Width = width;
            this.Height = height;
            this.Mass = mass;
            this.fastMoving = fastMoving;
            this.Category = category;
            this.Force = Vector2.Zero;
        }

        public void Bind(Simulation simulation)
        {
            if (!simulation.Contains(this))
            {
                throw new InvalidBindingException();
            }

            if (!Equals(this.Body, default(Body)))
            {
                throw new AlreadyBoundException();
            }

            

            float fWidth = width;
            float fHeight = height;
            float density = Mass.HasValue ? (float)this.Mass / (fWidth * fHeight) : 1;

            this.Body = BodyFactory.CreateRectangle(simulation.World, fWidth, fHeight, density);
            this.Body.BodyType = Mass.HasValue ? BodyType.Dynamic : BodyType.Static;
            this.Body.IsBullet = this.fastMoving;
            this.Body.FixedRotation = true;

            this.Body.LinearVelocity = this.preBindVelocity;
            this.Body.Position = this.preBindPosition;


            this.Body.CollidesWith = Collisions;
            this.Body.CollisionCategories = (FarseerPhysics.Dynamics.Category)Category;

        }

        public void PreSimulation()
        {
            if (Equals(this.Body, default(Body)))
            {
                throw new NotBoundException();
            }
            this.Body.ApplyForce(this.force);
        }

        public bool Matches(PhysicsObject obj)
        {
            return obj == this;
        }


    }

    public class AlreadyBoundException : ApplicationException
    {
        public AlreadyBoundException() {}
        public AlreadyBoundException(string message) {}
        public AlreadyBoundException(string message, Exception inner) {}
 
        protected AlreadyBoundException(SerializationInfo info,
            StreamingContext context) {}
    }

    public class NotBoundException : ApplicationException
    {
        public NotBoundException() {}
        public NotBoundException(string message) {}
        public NotBoundException(string message, Exception inner) {}
 
        protected NotBoundException(SerializationInfo info,
            StreamingContext context) {}
    }

    public class InvalidBindingException : ApplicationException
    {
        public InvalidBindingException() { }
        public InvalidBindingException(string message) { }
        public InvalidBindingException(string message, Exception inner) { }

        protected InvalidBindingException(SerializationInfo info,
            StreamingContext context) { }
    }
}
