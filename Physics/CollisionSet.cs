﻿using System.Collections.Generic;

namespace Physics
{
    public class CollisionSet : HashSet<CollisionEvent>
    {
        public List<CollisionEvent> GetCollisionsOf(IEventParticipant object1, IEventParticipant object2) {
            List<CollisionEvent> ret = new List<CollisionEvent>();

            foreach (CollisionEvent evt in this)
            {
                // Check if colliding objects match example objects provided in template, in any order.
                if ((object1.Matches((PhysicsObject)evt.Object1) && object2.Matches((PhysicsObject)evt.Object2)) ||
                    (object1.Matches((PhysicsObject)evt.Object2) && object2.Matches((PhysicsObject)evt.Object1)))
                {
                    ret.Add(evt);
                }
            }

            return ret;
        }

        public List<CollisionEvent> GetCollisionsOf(Category cat1, Category cat2)
        {
            List<CollisionEvent> ret = new List<CollisionEvent>();

            foreach (CollisionEvent evt in this)
            {
                // Check if colliding objects match example objects provided in template, in any order.
                if ((cat1.Equals(((PhysicsObject)evt.Object1).Category) && cat2.Equals(((PhysicsObject)evt.Object2).Category)) ||
                    (cat1.Equals(((PhysicsObject)evt.Object2).Category) && cat2.Equals(((PhysicsObject)evt.Object1).Category)))
                {
                    ret.Add(evt);
                }
            }

            return ret;
        }

        public List<CollisionEvent> GetCollisionsOf(Category cat, IEventParticipant obj)
        {
            List<CollisionEvent> ret = new List<CollisionEvent>();

            foreach (CollisionEvent evt in this)
            {
                // Check if colliding objects match example objects provided in template, in any order.
                if ((cat.Equals(((PhysicsObject)evt.Object1).Category) && obj.Matches(((PhysicsObject)evt.Object2))) ||
                    (cat.Equals(((PhysicsObject)evt.Object2).Category) && obj.Matches(((PhysicsObject)evt.Object1))))
                {
                    ret.Add(evt);
                }
            }

            return ret;
        }
    }
}
