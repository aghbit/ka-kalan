﻿namespace Physics
{
    public class CollisionEvent
    {
        public PhysicsObject Object1 { get; private set; }
        public PhysicsObject Object2 { get; private set; }
       
        public CollisionEvent(PhysicsObject object1, PhysicsObject object2)
        {
            if (object1.GetHashCode() < object2.GetHashCode())
            {
                this.Object1 = object1;
                this.Object2 = object2;
            }
            else
            {
                this.Object1 = object2;
                this.Object2 = object1;
            }
        }
    }
}
