﻿namespace Physics
{
    /// <summary>
    /// Generalisation of event participants: a concrete PhysicsObject or a Category of them.
    /// </summary>
    public interface IEventParticipant
    {
        /// <summary>
        /// Checks if given PhysicsObject can be represented by this generalisation.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Matches(PhysicsObject obj);
    }
}
