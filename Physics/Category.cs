﻿namespace Physics
{
    public enum Category
    {
        Walls=FarseerPhysics.Dynamics.Category.Cat1,
        Players=FarseerPhysics.Dynamics.Category.Cat2,
        Targets=FarseerPhysics.Dynamics.Category.Cat3,
        Bullets=FarseerPhysics.Dynamics.Category.Cat4,
        Pickup=FarseerPhysics.Dynamics.Category.Cat5
    }
}
