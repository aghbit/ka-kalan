﻿namespace KalanServer.ServerLogic
{
    public class ServerLaunchOptions
    {
        public Game1 Game { get; set; }
        public string MapName { get; set; }
        public ServerLaunchOptions(Game1 _game, string _mapName)
        {
            Game = _game;
            MapName = _mapName;
        }
    }
}
