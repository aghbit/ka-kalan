﻿using System;
using System.Text;

namespace KalanServer
{
    class ConsoleBufferer
    {
        private readonly StringBuilder _buf = new StringBuilder();
        public string ReadLine()
        {
            if (!Console.KeyAvailable)
                return null;
            var key = Console.ReadKey();
            if (key.Key == ConsoleKey.Enter)
            {
                Console.WriteLine();
                string retbuf = _buf.ToString();
                _buf.Clear();
                return retbuf;
            }

            _buf.Append(key.KeyChar);
            return null;
        }
    }
}