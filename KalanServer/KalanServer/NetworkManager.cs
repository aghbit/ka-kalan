﻿using System.Collections.Generic;
using Kalan.Net;
using Net.Net.Types;
using Protocol;

namespace KalanServer.ServerLogic
{
    public class NetworkManager
    {
        private ServerLaunchOptions options;
        private NetworkServer server;

        public NetworkManager(ServerLaunchOptions options)
        {
            // TODO: Complete member initialization
            this.options = options;
            NetworkFactory networkFactory = new NetworkFactory(NetConfigConstants.HOST_IP, NetConfigConstants.PORT);
            server = (NetworkServer) networkFactory.CreateServer();
            server.StartServer();
        }

        public IEnumerable<NetworkMessage> GetMessages()
        {
            NetworkMessage msg;

            server.Listen();

            while ((msg = server.Poll()) != null)
            {
                yield return msg;
            }
        }

        public void RejectPlayer(string name, NetEndpointId id, string reason)
        {
            Logger.Log(LoggingType.Normal, "Player {0} kicked, reason: {1}", name, reason);

            var rp = new RejectPacket() { message = new List<char>(reason) };
            server.SendPacket(rp, 0, SendOptions.ReliableInOrder, id);
        }

        public void SendWorldState(NetEndpointId id, BaseGamestatePacket packet)
        {
            server.SendPacket(packet, 0, SendOptions.Reliable, id);
        }
    }
}
