﻿using System.Linq;
using System.Text;
using KalanServer.ServerLogic;
using Net.Net.Types;
using Protocol;
using WorldState;

namespace KalanServer
{
    public class ServerDispatcher
    {
        private readonly Server _sv;
        private readonly NetworkManager _netMgr; 

        public ServerDispatcher(Server sv, NetworkManager netMgr)
        {
            _sv = sv;
            _netMgr = netMgr;
        }

        public bool DispatchPacket(NetworkMessage msg)
        {
            Logger.Log(LoggingType.Debug, "Got packet: {0}", msg.PacketType);

            switch (msg.PacketType)
            {
                case PacketType.HelloPacket:
                    return ProcessHelloPacket(msg, msg.Packet as HelloPacket);
                case PacketType.BaseGamestatePacket:
                    return ProcessBaseGamestatePacket(msg, msg.Packet as BaseGamestatePacket);
                case PacketType.UpdateGamestatePacket:
                    return ProcessUpdateGamestatePacket(msg, msg.Packet as UpdateGamestatePacket);
                case PacketType.RejectPacket:
                    return ProcessRejectPacket(msg, msg.Packet as RejectPacket);
                case PacketType.PlayerUpdatePacket:
                    return ProcessPlayerUpdatePacket(msg, msg.Packet as PlayerUpdatePacket);
            }

            return false;
        }
        private bool ProcessHelloPacket(NetworkMessage msg, HelloPacket packet)
        {
            var sb = new StringBuilder(packet.name.Count);
            packet.name.ForEach(a => sb.Append(a));
            string name = sb.ToString();

            if (_sv.WorldState.GetActivePlayers().Any(a => a.Name == name))
            {
                _netMgr.RejectPlayer(name, msg.Id, "Duplicate name");
                return false;
            }

            //TODO: Max Clients?

            var newPlayer = _sv.AddPlayer(msg.Id, name);

            if (newPlayer == null)
            {
                _netMgr.RejectPlayer(name, msg.Id, "Server full");
                return false;
            }

            Logger.Log(LoggingType.Normal, "Client {0} joined, endpoint: {1}", name, msg.Id);
            return true;
        }

        private bool ProcessBaseGamestatePacket(NetworkMessage msg, BaseGamestatePacket packet)
        {
            return false;
        }

        private bool ProcessUpdateGamestatePacket(NetworkMessage msg, UpdateGamestatePacket packet)
        {
            return false;
        }

        private bool ProcessRejectPacket(NetworkMessage msg, RejectPacket packet)
        {
            return false;
        }

        private bool ProcessPlayerUpdatePacket(NetworkMessage msg, PlayerUpdatePacket packet)
        {
            var player = _sv.PlayerByEndpoint(msg.Id);

            if (player == null)
                return false;

            Logger.Log(LoggingType.Debug, "PlayerUpdate from {0}, flags = {1}", player.Name, packet.flags);

            if((packet.flags & PlayerUpdateFlags.ChangeWeaponForward) == PlayerUpdateFlags.ChangeWeaponForward)
                player.NetPlayer.eq = player.NextGun().Type;

            if ((packet.flags & PlayerUpdateFlags.ChangeWeaponBackward) == PlayerUpdateFlags.ChangeWeaponBackward)
                player.NetPlayer.eq = player.PreviousGun().Type;

            if ((packet.flags & PlayerUpdateFlags.Move) == PlayerUpdateFlags.Move)
            {
                Logger.Log(LoggingType.Debug, "Player {0} wants to move, move dir {1}", player.Name, packet.move_dir);
                _sv.Slog.HandlePlayerMove(player.ServerId, packet.move_dir);
            }

            player.NetPlayer.flags &= ~PlayerFlags.Jump;

            if ((packet.flags & PlayerUpdateFlags.Jump) == PlayerUpdateFlags.Jump)
            {
                Logger.Log(LoggingType.Debug, "Player {0} wants to jump", player.Name);

                _sv.Slog.HandlePlayerJump(player.ServerId);
            }

            if ((packet.flags & PlayerUpdateFlags.Shoot) == PlayerUpdateFlags.Shoot)
            {
                Logger.Log(LoggingType.Debug, "Player {0} wants to shoot", player.Name);
                _sv.Slog.HandlePlayerShoot(player.ServerId);
            }

            if ((packet.flags & PlayerUpdateFlags.LowerGun) == PlayerUpdateFlags.LowerGun)
            {
                Logger.Log(LoggingType.Debug, "Player {0} want to lower his gun", player.Name);
                _sv.Slog.HandlePlayerLowerGun(player.ServerId);
            }

            if ((packet.flags & PlayerUpdateFlags.RaiseGun) == PlayerUpdateFlags.RaiseGun)
            {
                Logger.Log(LoggingType.Debug, "Player {0} want to raise his gun", player.Name);
                _sv.Slog.HandlePlayerRaiseGun(player.ServerId);
            }

            return true;
        }
		
	}

}
