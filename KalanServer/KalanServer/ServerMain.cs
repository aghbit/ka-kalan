﻿using System;
using System.Collections.Generic;
using KalanServer.ServerLogic;
using Microsoft.Xna.Framework.Graphics;
using Net.Net.Types;
using ServerLogic;
using WorldState;

namespace KalanServer
{
    public class ServerMain
    {
        NetworkManager _netManager;
        Server _sv;
        private ConsoleBufferer _cons = new ConsoleBufferer();



        public bool DoInit(ServerLaunchOptions options)
        {
            if (!InitServer(options))
                return false;

            _netManager = new NetworkManager(options);
            var worldState = new Worldstate(options.Game.Content.Load<Texture2D>(options.MapName));
            var slog = new ServerLogicAPI(worldState);
            _sv = new Server(options, worldState, _netManager, slog);

            return true; 
        }

        public void ExtGameLoop(TimeSpan timeSpan)
        {
            DoGameLoop(_netManager.GetMessages(), timeSpan);
        }

        void ProcessConsole()
        {
            string cmd = _cons.ReadLine();

            if (cmd == null)
                return;

            switch (cmd)
            {
                case "/hello":
                    Logger.Log(LoggingType.Normal, "Yo!");
                    break;
                case "/quit":
                    foreach (var p in _sv.GetActivePlayers())
                        _netManager.RejectPlayer(p.Name, p.Endpoint, "Server closing.");
                    Environment.Exit(0);
                    break;
            }
        }

        private void DoGameLoop(IEnumerable<NetworkMessage> events, TimeSpan timeSpan)
        {
            ProcessConsole();

            foreach (var ev in events)
                _sv.ProcessServerEvent(ev);

            _sv.AdvanceGameFrame(_netManager, timeSpan);

        }

        private bool InitServer(ServerLaunchOptions options)
        {
            return true;
        }
    }
}
