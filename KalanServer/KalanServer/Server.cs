﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kalan.Net;
using KalanServer.ServerLogic;
using Net.Net.Types;
using Protocol;
using ServerLogic;
using WorldState;

namespace KalanServer
{
    public class Server
    {
        public const int MaxPlayers = 64;

        public ServerLaunchOptions ServerLaunchOptions { get; private set; }
        public Worldstate WorldState { get; private set; }
        private Dictionary<NetEndpointId, ServerPlayer> _players;
        private Dictionary<int, ServerPlayer> _playersById; 
        public ServerDispatcher ServerDispatcher { get; private set; }
        public ServerLogicAPI Slog { get; private set; }
        public Server(ServerLaunchOptions slo, Worldstate ws, NetworkManager _netMgr, ServerLogicAPI slog)
        {
            _players = new Dictionary<NetEndpointId, ServerPlayer>();
            _playersById = new Dictionary<int, ServerPlayer>();
            WorldState = ws;
            ServerLaunchOptions = slo;
            ServerDispatcher = new ServerDispatcher(this, _netMgr);
            Slog = slog;
        }

        public ServerPlayer AddPlayer(NetEndpointId endpoint, string name)
        {
            if (_players.Count == MaxPlayers)
                return null;

            int nextId = Enumerable.Range(0, MaxPlayers).Except(_playersById.Keys).First();
            var svPlayer = new ServerPlayer(endpoint, nextId, name);
            _players[endpoint] = svPlayer;
            _playersById[nextId] = svPlayer;
            Slog.OnPlayerSpawn(svPlayer);

            return svPlayer;
        }

        internal void ProcessServerEvent(NetworkMessage ev)
        {
            switch (ev.Status)
            {
                case ClientStatus.Joined:
                    break;
                case ClientStatus.Left:
                    var pl = _players[ev.Id];

                    if (pl != null)
                    {
                        Logger.Log(LoggingType.Normal, "Player {0} left", pl.Name);
                        WorldState.RemovePlayer(pl.ServerId);
                        _players.Remove(ev.Id);
                    }

                    break;
                case ClientStatus.Playing:
                    ServerDispatcher.DispatchPacket(ev);
                    break;
            }
        }

        internal void AdvanceGameFrame(NetworkManager networkManager, TimeSpan timeSpan)
        {
            Slog.HandleCollisions(timeSpan);
           
            BaseGamestatePacket bgsp = new BaseGamestatePacket();
            bgsp.gs = new UpdateGamestatePacket();
            bgsp.map_name = new List<char>(WorldState.Map.Name);
            bgsp.map_size = new vec2() {x = WorldState.Map.SizeX, y = WorldState.Map.SizeY};
            bgsp.gs.players = WorldState.GetActivePlayers().Select(a => a.NetPlayer).ToList();
            bgsp.gs.events = new List<NetEvent>();
            bgsp.gs.phys_objects = new List<NetPhysObject>(WorldState.NetConv_GetActiveObjects());

            foreach (var pl in _players.Values)
            {
                if (pl.TimeOfLastPacket.AddMilliseconds(10) < DateTime.Now)
                {
                    networkManager.SendWorldState(pl.Endpoint, bgsp);
                    pl.TimeOfLastPacket = DateTime.Now;
                }
            }
        }

        public ServerPlayer PlayerByEndpoint(NetEndpointId endpointId)
        {
            return _players.ContainsKey(endpointId) ? _players[endpointId] : null;
        }

        public List<ServerPlayer> GetActivePlayers()
        {
            return _players.Values.ToList();
        }
    }
}
