﻿using System;
using System.Globalization;

namespace KalanServer
{
    [Flags]
    enum LoggingType
    {
        Normal = 1,
        Debug = 2,
        Physics = 4
    };

    class Logger
    {
        const LoggingType target = LoggingType.Debug | LoggingType.Normal | LoggingType.Physics;

        public static void Log(LoggingType type, string msg, params object[] args)
        {
            if ((type & target) == type)
                Console.WriteLine(DateTime.Now.ToString("[HH:mm:ss.fff] ", CultureInfo.InvariantCulture) + msg, args);
        }
    }
}
