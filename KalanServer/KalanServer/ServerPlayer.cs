﻿using System;
using Net.Net.Types;
using WorldState;

namespace KalanServer
{
    public class ServerPlayer : Player
    {
        public NetEndpointId Endpoint { get; set; }
        public DateTime LastActivity { get; set; }
        public DateTime TimeOfLastPacket { get; set; }
        
        public bool ShouldKickForInactivity()
        {
            return (DateTime.Now - LastActivity).TotalSeconds > 60; //Magic number <3
        }

        public ServerPlayer(NetEndpointId endpoint, int id, string name) : base (id, name)
        {
            Endpoint = endpoint;
            TimeOfLastPacket = DateTime.MinValue;
            LastActivity = DateTime.Now;
        }
    }
}
