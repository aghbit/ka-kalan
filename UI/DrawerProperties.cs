﻿using Microsoft.Xna.Framework.Graphics;

namespace UI
{
    public class DrawerProperties
    {
        public SpriteSortMode SpriteSortMode { get; private set; }
        public BlendState BlendState { get; private set; }
        public SamplerState SamplerState { get; private set; }
        public DepthStencilState DepthStencilState { get; private set; }
        public RasterizerState RasterizerState { get; private set; }

        public DrawerProperties()
        {
            SpriteSortMode = SpriteSortMode.Deferred;
            BlendState = BlendState.AlphaBlend;
            SamplerState = SamplerState.LinearClamp;
            DepthStencilState = DepthStencilState.None;
            RasterizerState = RasterizerState.CullCounterClockwise;
        }
    }
}
