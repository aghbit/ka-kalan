﻿namespace UI
{
    public interface IWorldDrawer
    {
        void DrawWorld(World world, DrawerProperties drawerProperties, ConvertUnit convertUnit);
    }
}
