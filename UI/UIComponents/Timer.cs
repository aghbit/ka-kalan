﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using IDrawable = UI.WorldObjects.IDrawable;

namespace UI.UIComponents
{
    public class Timer : IDrawable
    {

        private SpriteFont _font1;
        private DateTime _time;
        private bool _run;
        public TimeSpan Duration { get; set; }
        public Vector2 Position { get; set; }

        public Timer(TimeSpan time, Vector2 position)
        {
            this.Position = position;
            Duration = time;
            _run = false;
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            string output = (_run ? _time.TimeOfDay - DateTime.Now.TimeOfDay : Duration).ToString(@"mm\:ss");
            Vector2 fontOrigin = _font1.MeasureString(output) / 2;
            spriteBatch.DrawString(_font1, output, Position, Color.Violet, 0, fontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        }

        public bool TimeOut()
        {
            if ((_run) && (_time < DateTime.Now))
            {
                _run = false;
                Duration = TimeSpan.Zero;
                return true;
            }
            return false;
        }

        public void Start()
        {
            _run = true;
            _time = DateTime.Now.Add(Duration);
        }

        public void Stop()
        {
            _run = false;
        }

        public void LoadContent()
        {
            _font1 = ContentLoaderRetriever.GetContentLoader().Load<SpriteFont>("Courier New");
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 3; }
        }
    }
}
