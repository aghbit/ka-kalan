﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using IDrawable=UI.WorldObjects.IDrawable;

namespace UI.UIComponents
{
    public class Score : global::UI.WorldObjects.IDrawable
    {
        public int score { get; set; } = 0;
        private SpriteFont _font1;
        public Vector2 Position { get; set; }

        public Score(Vector2 position)
        {
            this.Position = position;
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            string output = "Score: " + score.ToString();
            Vector2 fontOrigin = new Vector2(0,0);
            spriteBatch.DrawString(_font1, output, Position, Color.Violet, 0, fontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        }


        public Vector2 TextSize()
        {
            return _font1.MeasureString("Score: " + score.ToString());
        }

        public void LoadContent()
        {
            _font1 = ContentLoaderRetriever.GetContentLoader().Load<SpriteFont>("Courier New");
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 3; }
        }
    }
}
