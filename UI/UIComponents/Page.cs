﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ruminate.GUI.Framework;

namespace UI.UIComponents
{
    public abstract class Page
    {
        protected Color Color { get; set; }
        protected Gui PageRoot { get; set; }
        protected Game Game { get; set; }

        public virtual void Init(Game game, Texture2D texture, string mapping, SpriteFont font)
        {
            this.Game = game;
        }

        public abstract void Update(GameTime time);
        public abstract void Draw();
        public abstract void Disable();
        public abstract void Enable();
    }
}
