﻿using Microsoft.Xna.Framework.Graphics;
using Model = UI.UIComponents.Model;

namespace UI.UIComponents
{
    public class UIDrawer : IUIDrawer
    {
        private SpriteBatch _spriteBatch;
        public UIDrawer(SpriteBatch spriteBatch)
        {
            _spriteBatch = spriteBatch;
        }

        public void DrawUI(Model model, ConvertUnit convertUnit)
        {
            model.Score.Draw(_spriteBatch, convertUnit);
            model.Timer.Draw(_spriteBatch, convertUnit);
        }

    }
}
