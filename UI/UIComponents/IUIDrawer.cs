﻿namespace UI.UIComponents
{
    public interface IUIDrawer
    {
        void DrawUI(Model model, ConvertUnit convertUnit);
    }
}
