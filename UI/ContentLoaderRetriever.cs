﻿using System;
using Microsoft.Xna.Framework.Content;

namespace UI
{
    public class ContentLoaderRetriever
    {
        private static ContentManager ContentManager;
        private static Boolean _hasBeenInitialized;

        private ContentLoaderRetriever()
        {
            // block the public default constuctor
        }

        public static void Initialize(ContentManager contentManager)
        {
            ContentManager = contentManager;
            _hasBeenInitialized = true;
        }

        public static ContentManager GetContentLoader()
        {
            if (_hasBeenInitialized)
            {
                return ContentManager;
            }
            throw new InvalidOperationException();
        }
    }
}
