﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using UI.WorldObjects;
using UI.WorldObjects.Pickups;
using UI.WorldObjects.Targets;

namespace UI
{
    public class World
    {
        public List<Player> Players { get; set; }
        public Background Background { get; set; }
        public List<PhysicsObject> NonPlayerObjects { get; private set; }
        public List<Target> Targets { get; set; }
        public List<Wall> Walls { get; set; }
        public Vector2 MapSize { get; set; }
        public List<Bullet> Bullets { get; set; }
        public List<DestroyedTarget> DestroyedTargets { get; set; }
        public List<Pickup> Pickups { get; set; } 
        public World()
        {
            Players = new List<Player>();
            Background = new Background();
            NonPlayerObjects = new List<PhysicsObject>();
            Walls = new List<Wall>();
            MapSize = new Vector2();
            Bullets = new List<Bullet>();
			Targets = new List<Target>();
            Pickups = new List<Pickup>();
            DestroyedTargets = new List<DestroyedTarget>();
        }
    }
}
