﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Protocol;
using UI.WorldObjects.Weapon;

namespace UI.WorldObjects
{
    public class Player : IAnimated
    {
        public Vector2 Velocity { get; set; }
        public double Angle { get; set; }
        public Vector2 Position { get; set; }
        public float w { get; set; }
        public float h { get; set; }
        public SpriteEffects Direction { get; set; }
        public Equipment Eq { get; set; }
        public float WeaponAngle { get; set; } = 0.0f;

        private Texture2D _texture;
        private string _textureFilename;
        private Color _tintColor;
        private int _frame;
        private int _frameSpeed;
        private Rectangle _frameRect;
        private int _tickCount;
        private static readonly Container Weapons = new Container(); 

        public Player(string textureFilename, Vector2 position, double angle, bool local)
        {
            _textureFilename = textureFilename;
            Position = position;
            Angle = angle % Math.PI;
            _tintColor = Color.White;
            if(local) _tintColor = Color.Red;
            _frame = 0;
            _frameSpeed = 300;
            _frameRect.Height = 256;
            _frameRect.Width = 128;
            _frameRect.X = 0;
            _frameRect.Y = 0;
            _tickCount = 0;
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit )
        {
            // Draw method parameters:
            // Players texture, Players Position, source rectangle(not needed), tint color, Players rotation (not yet implemented), 
            // origin vector(??), scale vector, sprite effects, layer depth.
            var pos = new Vector2(Position.X - w/2, Position.Y - h/2);
            spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(pos), _frameRect, _tintColor, 0.0f, Vector2.Zero, new Vector2(convertUnit.ScaleToDraw(w / _frameRect.Width), convertUnit.ScaleToDraw(h / _frameRect.Height)), Direction, 0.0f);

            var weaponPos = new Vector2(Position.X, Position.Y - h / 8);
            var weaponSize = new Vector2(w / _frameRect.Width, h / _frameRect.Height);
            Weapons.GetGun(Eq).Draw(spriteBatch, weaponPos, weaponSize, WeaponAngle, Direction, convertUnit);
        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(_textureFilename);
            Weapons.LoadContent();
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 2; }
        }

        public void AdvanceFrame()
        {
            if(Velocity.Y != 0)
            {
                _frameRect.X = _frameRect.Width;
                _frameRect.Y = _frameRect.Height;
            }
            else if(Velocity.X == 0)
            {
                _frameRect.X = _frameRect.Width;
                _frameRect.Y = 0;
                _tickCount = 0;
            }
            else if (_tickCount * Velocity.X > _frameSpeed || _tickCount * Velocity.X < -_frameSpeed)
            {
                _frame += 1;
                if (_frame >= 8) _frame = 0;
                _frameRect.X = 0;
                _frameRect.Y = _frame * _frameRect.Height;   
                _tickCount = 0;
            }
            _tickCount++;
        }
    }
}
