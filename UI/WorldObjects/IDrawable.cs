﻿using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects
{
    public interface IDrawable
    {
        void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit);

        void LoadContent();

        bool IsVisible { get; }

        int DrawOrder { get; }
    }
}