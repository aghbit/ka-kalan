﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects.Weapon
{
    internal class Shotgun : IWeapon
    {
        private const string TextureFilename = "Shotgun";
        private Texture2D _texture;
        private Color _tintColor = Color.White;

        public void Draw(SpriteBatch spriteBatch, Vector2 position, Vector2 size, float angle, SpriteEffects direction, ConvertUnit convertUnit)
        {
            var center = new Vector2((float)_texture.Width / 2, (float)_texture.Height / 2);
            angle = direction == SpriteEffects.FlipHorizontally ? angle : -angle;
            spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(position), null, _tintColor, angle, center, new Vector2(convertUnit.ScaleToDraw(size.X), convertUnit.ScaleToDraw(size.Y)), direction, 0.0f);
        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(TextureFilename);
        }
    }
}
