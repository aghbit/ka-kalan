﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects.Weapon
{
    internal class NoWeapon : IWeapon
    {
        
        public void Draw(SpriteBatch spriteBatch, Vector2 position, Vector2 size, float angle, SpriteEffects direction, ConvertUnit convertUnit)
        {
            return;
        }

        public void LoadContent()
        {
            return;
        }
    }
}
