using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects.Weapon
{
    public interface IWeapon
    {
        void Draw(SpriteBatch spriteBatch, Vector2 position, Vector2 size, float angle, SpriteEffects direction, ConvertUnit convertUnit);

        void LoadContent();
    }
}
