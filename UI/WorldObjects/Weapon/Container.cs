﻿using Protocol;

namespace UI.WorldObjects.Weapon
{
    public class Container
    {
        private Shotgun _shotgun = new Shotgun();
        private Pistol _pistol = new Pistol();
        private NoWeapon _noWeapon = new NoWeapon();

        public IWeapon GetGun(Equipment type)
        {
            switch (type)
            {
                case Equipment.Pistol:
                    return _pistol;
                case Equipment.Shotgun:
                    return _shotgun;
                default: return _noWeapon;
            }
        }

        public void LoadContent()
        {
            _shotgun.LoadContent();
            _pistol.LoadContent();
            _noWeapon.LoadContent();
        }
    }
}
