﻿namespace UI.WorldObjects
{
    public interface IDestructable
    {
        bool ToDestroy();
    }
}