﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects
{
    public class Bullet : WorldObjects.IDrawable
    {
        public Vector2 Position { get; set; }

        private const string _textureFilename = "bullet";
        private readonly Color _color = Color.White;
        private Texture2D _texture;

        public Bullet(Vector2 position)
        {
            Position = position;
        }

        public int DrawOrder
        {
            get { return 4; }
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            // Draw method parameters:
            // Bullet texture, Bullet Position, source rectangle(not needed), tint color, rotation (not needed for bullet), 
            // origin vector(??), scale vector, sprite effects, layer depth.
            var pos = new Vector2(Position.X - _texture.Width / 2, Position.Y - _texture.Height / 2);
            spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(pos), null, _color, 0.0f, Vector2.Zero, new Vector2(convertUnit.ScaleToDraw(1),convertUnit.ScaleToDraw(1)), SpriteEffects.None, 0.0f);
            
        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(_textureFilename);
        }

        public bool IsVisible
        {
            get { return true; }
        }

    }
}
