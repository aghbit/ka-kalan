﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects.Pickups
{
    public abstract class Pickup : WorldObjects.IDrawable
    {
        private readonly Color _color = Color.White;
        private Vector2 _position;
        private short _id;
        private float _w;
        private float _h;
        protected String textureFilename;
        private Texture2D _texture;

        public Pickup(Vector2 position, short id, float w, float h)
        {
            this._w = w;
            this._h = h;
            this._position = position;
            this._id = id;
        }
            
        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            var pos = new Vector2(_position.X - _w / 2, _position.Y - _h / 2);
            spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(pos), null, _color, 0.0f, Vector2.Zero, new Vector2(convertUnit.ScaleToDraw(_w / _texture.Width), convertUnit.ScaleToDraw(_h / _texture.Height)), SpriteEffects.None, 0.0f);
        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(textureFilename);
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 5; }
        }


    }
}
