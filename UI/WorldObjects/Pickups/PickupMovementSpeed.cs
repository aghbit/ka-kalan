﻿using Microsoft.Xna.Framework;

namespace UI.WorldObjects.Pickups
{
    public class PickupMovementSpeed : Pickup
    {
        public PickupMovementSpeed(Vector2 position, short id, float w, float h) : base(position, id, w, h)
        {
            textureFilename = "box_speed";
        }
    }
}
