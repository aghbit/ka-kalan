﻿using Microsoft.Xna.Framework;

namespace UI.WorldObjects.Pickups
{
    public class PickupShotgun : Pickup
    {
        public PickupShotgun(Vector2 position, short id, float w, float h) : base(position, id, w, h)
        {
            textureFilename = "box_shotgun";
        }
    }
}
