﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects
{
    public class Background : WorldObjects.IDrawable
    {
        private Texture2D _texture;
        private readonly string _filename;
        private readonly Vector2 _position;
        private readonly Vector2 _size;
        private readonly Color _tintColor;

        public Background() { }

        public Background(string filename)
        {
            _filename = filename;
            _position = new Vector2(0.0f, 0.0f);
            _tintColor = Color.White;
            _size = new Vector2(1.0f, 1.0f); //it determinates how much to stretch background according to map size. 
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
             //using convertUnit.MapSize here is risky, because there is no guarantee that it is MapSize sent by server. It is needed to decide whether to redesigning some convertUnit in future or change drawing behaviour of background.
             spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(_position), null, _tintColor, 0, Vector2.Zero, new Vector2(convertUnit.ScaleToDraw(convertUnit.MapSize.X * _size.X / _texture.Width), convertUnit.ScaleToDraw(convertUnit.MapSize.Y * _size.Y / _texture.Height)), SpriteEffects.None, 0.6f);
        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(_filename);
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 0; }
        }
    }
}
