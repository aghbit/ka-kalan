﻿namespace UI.WorldObjects
{
    interface IAnimated : IDrawable
    {
        void AdvanceFrame();
    }
}
