﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects.Targets
{
    public class DestroyedTarget : IAnimated, IDestructable
    {
        private readonly Color _color = Color.White;
        private Vector2 _position;
        private float _w;
        private float _h;
        private String textureFilename = "target_destruction";
        private Texture2D _texture;
        
        private int _tickCount;
        private Rectangle _frameRect;
        private int _frame;
        private bool _end;

        public DestroyedTarget(Vector2 position, float w, float h)
        {
            this._w = w;
            this._h = h;
            this._position = position;
            this._tickCount = 0;
            this._frame = 0;
            this._frameRect.Height = 125;
            this._frameRect.Width = 125;
            this._frameRect.X = 0;
            this._frameRect.Y = 0;
            this._end = false;
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            var pos = new Vector2(_position.X - _w / 2, _position.Y - _h / 2);
            spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(pos), _frameRect, _color, 0.0f, Vector2.Zero, new Vector2(convertUnit.ScaleToDraw(_w / _frameRect.Width), convertUnit.ScaleToDraw(_h / _frameRect.Height)), SpriteEffects.None, 0.0f);

        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(textureFilename);
        }

        public void AdvanceFrame()
        {
            if (_tickCount % 6 == 0)
            {
                _frameRect.X = 0;
                _frameRect.Y = _frame * _frameRect.Height;
                _tickCount = 0;
                _frame++;
                if (_frame >= 6)
                    _end = true;
            }
            _tickCount++;
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 4; }
        }

        public bool ToDestroy()
        {
            return _end;
        }
    }
}
