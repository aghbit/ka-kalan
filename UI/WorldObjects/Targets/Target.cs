﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects.Targets
{
    public class Target : WorldObjects.IDrawable
    {
        private readonly Color _color = Color.White;
        public Vector2 Position { get; private set; }
        public short Id { get; private set; }
        public float Width { get; private set; }
        public float Height { get; private set; }
        private String textureFilename = "Target";
        private Texture2D _texture;

        public Target(Vector2 position, short id, float w, float h)
        {
            this.Width = w;
            this.Height = h;
            this.Position = position;
            this.Id = id;
        }
            
        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            var pos = new Vector2(Position.X - Width / 2, Position.Y - Height / 2);
            spriteBatch.Draw(_texture, convertUnit.ScaleToDraw(pos), null, _color, 0.0f, Vector2.Zero, new Vector2(convertUnit.ScaleToDraw(Width / _texture.Width), convertUnit.ScaleToDraw(Height / _texture.Height)), SpriteEffects.None, 0.0f);

        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(textureFilename);
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 4; }
        }
    }
}
