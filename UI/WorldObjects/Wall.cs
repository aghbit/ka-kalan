﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UI.WorldObjects
{
    public class Wall : IDrawable
    {
        // Right now the wall will be represented as a line. To change it to rectangles or any other structure do those things:
        // 1. Change the appropriate fields (i.e. reclangle can be represented as Microsoft.Xna.Framework.Rectangle)
        // 2. Change how the structure is created in ClientDispatcher
        // 3. Change how the structure is drawn in DrawWorld method in WorldDrawer class.

        public Vector2 Begin { get; private set; }
        public Vector2 End { get; private set; }
        
        private readonly int _width;
        private readonly int _height;
        
        private const string TextureFilename = "wall";
        private readonly Color _color = Color.White;
        private Texture2D _texture;
        
        public Wall(Vector2 position, float width, float height)
        {
            Begin = new Vector2(position.X - width / 2, position.Y - height / 2);
            End = new Vector2(position.X + width / 2, position.Y + height / 2);
            _width = (int) width;
            _height = (int) height;
        }

        public void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit)
        {
            Vector2 b = convertUnit.ScaleToDraw(Begin);
            Vector2 e = convertUnit.ScaleToDraw(End);
            Rectangle rectangle = new Rectangle((int)b.X, (int)b.Y-3, (int)(e.X - b.X) + 1, (int)(e.Y - b.Y) + 7);
            spriteBatch.Draw(_texture, rectangle, null, _color, 0, Vector2.Zero, SpriteEffects.None, 0f);

        }

        public void LoadContent()
        {
            _texture = ContentLoaderRetriever.GetContentLoader().Load<Texture2D>(TextureFilename);
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public int DrawOrder
        {
            get { return 1; }
        }
    }
}
