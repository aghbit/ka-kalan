﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using UI.WorldObjects;

namespace UI
{
    public class WorldDrawer : IWorldDrawer
    {
        private readonly SpriteBatch _spriteBatch;

        public WorldDrawer(SpriteBatch spriteBatch)
        {
            _spriteBatch = spriteBatch;
        }

        public void DrawWorld(World world, DrawerProperties drawerProperties, ConvertUnit convertUnit)
        {

            _spriteBatch.Begin(drawerProperties.SpriteSortMode,
                                drawerProperties.BlendState,
                                drawerProperties.SamplerState,
                                drawerProperties.DepthStencilState,
                                drawerProperties.RasterizerState);

            world.DestroyedTargets.RemoveAll(destroyedTarget => destroyedTarget.ToDestroy());

            var objectsToAnimate = new List<IAnimated>();
            objectsToAnimate.AddRange(world.Players);
            objectsToAnimate.AddRange(world.DestroyedTargets);

            foreach (var obj in objectsToAnimate)
            {
                obj.AdvanceFrame();
            }

            var objects = new List<IDrawable>();
            objects.Add(world.Background);
            objects.AddRange(world.Walls);
            objects.AddRange(world.Players);
            objects.AddRange(world.NonPlayerObjects);
            objects.AddRange(world.Bullets);
            objects.AddRange(world.Targets);
            objects.AddRange(world.Pickups);
            objects.AddRange(world.DestroyedTargets);
            objects.Sort((a, b) => a.DrawOrder.CompareTo(b.DrawOrder));

            foreach (var obj in objects)
            {                          
                obj.Draw(_spriteBatch, convertUnit);
            }
            _spriteBatch.End();
        }
    }
}
