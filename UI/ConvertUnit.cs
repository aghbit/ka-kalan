﻿using System;
using Microsoft.Xna.Framework;

namespace UI
{
    public enum ConvertMode
    {
        None,
        MidAtCenter,
        FitMapToScreen,
        FitMapAndMid
    }

    public class ConvertUnit
    {
        public Vector2 MapSize { get; set; }
        public Vector2 WinSize { get; set; }
        public Vector2 Mid { get; set; }
        private ConvertMode _mode = ConvertMode.None;
        private float _ratio;
        private Vector2 _midPos;

        public void SetMode( ConvertMode mode)
        {
            _mode = mode;
            switch (_mode)
            {
                    case ConvertMode.None:
                    _ratio = 1f;
                    _midPos = Vector2.Zero;
                    break;
                    case ConvertMode.FitMapToScreen:
                    _ratio = Math.Min(WinSize.X / MapSize.X, WinSize.Y / MapSize.Y);
                    _midPos = Vector2.Zero;
                    break;
                    case ConvertMode.MidAtCenter:
                    _ratio = 1f;
                    _midPos = new Vector2(WinSize.X / 2 - Mid.X, WinSize.Y / 2 - Mid.Y);
                    break;
                    case ConvertMode.FitMapAndMid:
                    _ratio = Math.Min(WinSize.X / MapSize.X, WinSize.Y / MapSize.Y);
                    _midPos = new Vector2((WinSize.X / 2 - Mid.X)*_ratio, (WinSize.Y / 2 - Mid.Y)*_ratio);
                    break;
            }
        }

        public void Update()
        {
            SetMode(_mode);
        }

        public Vector2 ScaleToDraw(Vector2 a)
        {
            return new Vector2(a.X * _ratio +_midPos.X, a.Y * _ratio +_midPos.Y);
        }

        public float ScaleToDraw(float a)
        {
            return a * _ratio;
        }

        public Vector2 ScaleFromDraw(Vector2 a)
        {
            return new Vector2((a.X - _midPos.X)/_ratio, (a.Y - _midPos.Y)/_ratio);
        }

        public float ScaleFromDraw(float a)
        {
            return a / _ratio;
        }
    }
}
