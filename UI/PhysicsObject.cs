﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using IDrawable = UI.WorldObjects.IDrawable;

namespace UI
{
    public abstract class PhysicsObject : WorldObjects.IDrawable
    {
        protected Vector2 Position;

        protected PhysicsObject(Vector2 position)
        {
            Position = position;
        }

        public abstract void Draw(SpriteBatch spriteBatch, ConvertUnit convertUnit);

        public abstract void LoadContent();

        public abstract bool IsVisible { get; }

        public abstract int DrawOrder { get; }
    }
}